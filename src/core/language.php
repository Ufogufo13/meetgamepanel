<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;

/**
 * PufferPanel Core Language Class
 */
class Language {

	/**
	 * Constructor class for language
	 *
	 * @param string $language The language to load.
	 * @return void
	 */
	public function __construct($language){

		$this->_language = $language;

		if(!file_exists(dirname(__DIR__).'/lang/'.$language.'.json'))
			exit('Unable to load the required language file! lang/'.$language.'.json');

		$this->loaded_language = json_decode(file_get_contents(dirname(__DIR__).'/lang/'.$language.'.json'), true);

	}

	/**
	 * Returns the language value associated with a key. Graceful fallback to English if translated version doesn't exist.
	 *
	 * @param string $template The language key.
	 * @return string
	 */
	public function tpl($template){

		$template = str_replace(".", "_", $template);

		if(array_key_exists($template, $this->loaded_language))
			return $this->loaded_language[$template];
		else{

			if($this->_language == "en")
				return $template;
			else{

				$this->load_english = json_decode(file_get_contents(__DIR__.'/lang/en.json'), true);

					if(array_key_exists($template, $this->load_english))
						return $this->load_english[$template];
					else
						return $template;

			}

		}

	}

	/**
	 * Returns the loaded langauge as an array.
	 *
	 * @return array
	 */
	public function loadTemplates() {

		return $this->loaded_language;

	}

 }

 ?>