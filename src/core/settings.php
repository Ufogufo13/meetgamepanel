<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

/**
 * PufferPanel Core Settings Class File
 */
class Settings {

	/**
	 * Constructor class for building settings data.
	 *
	 * @return void
	 */
	public function __construct()
		{

			$this->settings = ORM::forTable('acp_settings')->findMany();

			foreach($this->settings as $this->setting)
				$this->_data[$this->setting->setting_ref] = $this->setting->setting_val;

			$this->nodes = ORM::forTable('nodes')->select('id')->select('node')->findMany();

			foreach($this->nodes as $this->node)
					$this->_dataNode[$this->node->id] = $this->node->node;

		}

	/**
	 * Function to retrieve various panel settings.
	 *
	 * @param string $setting The name of the setting for which you want the value.
	 * @return mixed This will return the column data for the setting, or if $setting was left blank all settings in an array.
	 */
	public function get($setting = null)
		{

			if(is_null($setting))
				return $this->_data;
			else
				return (array_key_exists($setting, $this->_data)) ? $this->_data[$setting] : '_notfound_';

		}

	/**
	 * Convert a node ID into a name for the node.
	 *
	 * @param int $id The ID of the node you want the name for.
	 * @return string The name of the node.
	 */
	public function nodeName($id)
		{

			return (array_key_exists($id, $this->_dataNode)) ? $this->_dataNode[$id] : 'unknown';

		}

}
?>