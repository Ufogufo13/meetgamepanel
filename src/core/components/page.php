<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core\Components;

/**
 * Page components trait to allow access in other classes and functions easily.
 */
trait Page {

	/**
	 * Redirects the user to a specified page automatically depending on headers.
	 *
	 * @param string $url The URL to redirect the user to.
	 * @return void
	 * @static
	 */
	public static function redirect($url) {

		if(!headers_sent()){
			header('Location: '.urldecode($url));
			exit();
		}else
			exit('<meta http-equiv="refresh" content="0;url='.urldecode($url).'"/>');

	}

	/**
	 * Generates a URL.
	 *
	 * @return string
	 * @static
	 */
	public static function genRedirect(){

		$https = (isset($_SERVER['HTTPS'])) ? 'https://' : 'http://';
		return $https.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

	}

	/**
	 * Fixes issue with twig not accepting empty array values
	 *
	 * @return array
	 * @static
	 */
	public static function twigGET() {

		$vars = $_GET;
		$return = array();
		foreach($vars as $id => $value){

			if(empty($value))
				$value = true;

			$return[$id] = $value;
		}

		return $return;

	}

}

?>