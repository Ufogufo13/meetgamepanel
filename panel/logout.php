<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token')) !== true)
	Components\Page::redirect('index.php');
else{

	/*
	 * Expire Session and Clear Database Details
	 */
	setcookie("pp_auth_token", null, time()-86400, '/');
	setcookie("pp_server_node", null, time()-86400, '/');
	setcookie("pp_server_hash", null, time()-86400, '/');

	$logout = ORM::forTable('users')->where(array('session_id' => $_COOKIE['pp_auth_token'], 'session_ip' => $_SERVER['REMOTE_ADDR']))->findOne();
	$logout->session_id = null;
	$logout->session_ip = null;
	$logout->save();

    $core->log->getUrl()->addLog(0, 1, array('auth.user_logout', 'Account logged out from '.$_SERVER['REMOTE_ADDR'].'.'));
	Components\Page::redirect('index.php');

}
?>
