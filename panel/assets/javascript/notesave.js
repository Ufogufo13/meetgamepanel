function noteSave() {
    $("#note_content").each(function(index, data){
        var element = $(this);
        var content = element.code();
        var noteID = element.attr("value");
        var noteName = document.getElementById("note_names").value;
        $.ajax({
            type: "POST",
            url: document.getElementById("master_url").value+"ajax/note_save.php",
            data: { method: "simple", note_content: content, note_id: noteID, note_name: noteName },
            success: function(data) {
                $("#note_save_results").slideUp(function(){
                    $("#note_save_results").html();
                    $("#note_save_results").html(data);
                    $("#note_save_results").fadeIn(function(){
                        $("#ss_active_spin").html('&rarr;');
                            return false;
                    });
                });
                var ownerID = document.getElementById("userrko").value;
                $.ajax({
                    type: "POST",
                    url: document.getElementById("master_url").value+"ajax/search_note.php",
                    data: { method: "simple", owner_id: ownerID },
                    success: function(data) {
                        $("#search_results_notes").slideUp(function(){
                            $("#search_results_notes").html();
                            $("#search_results_notes").html(data);
                            $("#search_results_notes").fadeIn(function(){
                                $("#ss_active_spin").html('&rarr;');
                                return false;
                            });
                        });
                    }
                });
                setTimeout(function(){ 
                    if(document.getElementById("note_save_results") !== null){
                        $("#note_save_results").slideUp(function(){
                            $("#note_save_results").html();
                            document.getElementById("note_save_results").innerHTML = null;
                        });
                    }
                }, 9000); 
            }
        });
    });
}
function noteAdd() {
    $("#note_content_add").each(function(index, data){
        var element = $(this);
        var content = element.code();
        var noteName = document.getElementById("note_names").value;
        $.ajax({
            type: "POST",
            url: document.getElementById("master_url").value+"ajax/note_save.php",
            data: { method: "simple", note_content: content, note_name: noteName },
            success: function(data) {
                $("#note_save_results").slideUp(function(){
                    $("#note_save_results").html();
                    $("#note_save_results").html(data);
                    $("#note_save_results").fadeIn(function(){
                        $("#ss_active_spin").html('&rarr;');
                            return false;
                    });
                });
                if(document.getElementById("edit_results_notes") !== null){
                    $("#edit_results_notes").slideUp(function(){
                        $("#edit_results_notes").html();
                        document.getElementById("edit_results_notes").innerHTML = null;
                    });
                }
                var ownerID = document.getElementById("userrko").value;
                $.ajax({
                    type: "POST",
                    url: document.getElementById("master_url").value+"ajax/search_note.php",
                    data: { method: "simple", owner_id: ownerID },
                    success: function(data) {
                        $("#search_results_notes").slideUp(function(){
                            $("#search_results_notes").html();
                            $("#search_results_notes").html(data);
                            $("#search_results_notes").fadeIn(function(){
                                $("#ss_active_spin").html('&rarr;');
                                return false;
                            });
                        });
                    }
                });
                setTimeout(function(){ 
                    if(document.getElementById("note_save_results") !== null){
                        $("#note_save_results").slideUp(function(){
                            $("#note_save_results").html();
                            document.getElementById("note_save_results").innerHTML = null;
                        });
                    }
                }, 9000); 
            }
        });
    });
}
function noteDelete() {
    if (confirm('Are you sure you want to delete this note?')) {
        $("#note_content").each(function(index, data){
            var element = $(this);
            var noteID = element.attr("value");
            $.ajax({
                type: "POST",
                url: document.getElementById("master_url").value+"ajax/note_delete.php",
                data: { method: "simple", note_id: noteID },
                success: function(data) {
                    $("#note_save_results").slideUp(function(){
                        $("#note_save_results").html();
                        $("#note_save_results").html(data);
                        $("#note_save_results").fadeIn(function(){
                            $("#ss_active_spin").html('&rarr;');
                                return false;
                        });
                    });
                    if(document.getElementById("edit_results_notes") !== null){
                        $("#edit_results_notes").slideUp(function(){
                            $("#edit_results_notes").html();
                            document.getElementById("edit_results_notes").innerHTML = null;
                        });
                    }
                    var ownerID = document.getElementById("userrko").value;
                    $.ajax({
                        type: "POST",
                        url: document.getElementById("master_url").value+"ajax/search_note.php",
                        data: { method: "simple", owner_id: ownerID },
                        success: function(data) {
                            $("#search_results_notes").slideUp(function(){
                                $("#search_results_notes").html();
                                $("#search_results_notes").html(data);
                                $("#search_results_notes").fadeIn(function(){
                                    $("#ss_active_spin").html('&rarr;');
                                    return false;
                                });
                            });
                        }
                    });
                    setTimeout(function(){ 
                        if(document.getElementById("note_save_results") !== null){
                            $("#note_save_results").slideUp(function(){
                                $("#note_save_results").html();
                                document.getElementById("note_save_results").innerHTML = null;
                            });
                        }
                    }, 9000); 
                }
            });
        });
    } else {}   
}
$("[name='note_id']").click(function(){
    var noteID = $(this).attr("id");
    $.ajax({
        type: "POST",
        url: document.getElementById("master_url").value+"ajax/edit_note.php",
        data: { method: "simple", note_id: noteID },
        success: function(data) {
            $("#edit_results_notes").slideUp(function(){
                $("#edit_results_notes").html();
                $("#edit_results_notes").html(data);
                $("#edit_results_notes").fadeIn(function(){
                    $("#ss_active_spin").html('&rarr;');
                        return false;
                });
            });
        }
    });
});

