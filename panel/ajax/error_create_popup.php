<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}


echo '<script language="JavaScript" src="'.$core->settings->get('assets_url').'javascript/errorcreate.js" type="text/javascript"></script>'; 

echo '
 <div class="well">
    <div class="row">
        <div class="form-group col-3 nopad">
            <label for="error_comment" class="control-label">Name</label>
            <div class="input-group">
                <input type="text" name="error_comment" id="error_comment" class="form-control" />
            </div>
        </div>
        <div class="form-group col-3 nopad">
            <label for="error_direcrory_selector" class="control-label">Error direcrory</label>
            <div class="radio">
                <input type="radio" name="error_direcrory_selector" id="error_direcrory_selector_1" value="1" />On this page
            </div>
        </div>
        <div class="form-group col-6 nopad-right"></br>
            <div class="radio">
                <input type="radio" name="error_direcrory_selector" id="error_direcrory_selector_0" value="0" />Other page:
            </div>
            <div class="input-group">
                <input type="text" name="error_directory_hand" id="error_directory_hand" class="form-control" />
            </div>
        </div>
        <input type="hidden" name="file_directory" id="current_dir" value="'.$_POST['file_url'].'"/>
        <button type="button" name="errorCreate" class="btn btn-primary" style="margin-top:5px;">Save</button>
    </div>
</div>
<div class="row">
    <div class="form-group col-12 nopad">
        <div id="error_add_result"></div>
    </div>
</div>
';
?>