<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

echo '<script language="JavaScript" src="'.$core->settings->get('assets_url').'javascript/notesave.js" type="text/javascript"></script>';

echo '
    <div class="row">
        <div class="form-group col-4 nopad">
            <label for="note_names" class="control-label">Name:</label>
            <div>
                <input type="text" autocomplete="off" name="note_names" class="form-control" id="note_names"/>
            </div>
        </div>
        <div class="col-5 nopad">
            <h3 style="float:right; margin-top:10px;">New Note</h3>
        </div>
    </div>
    <div name="note_content_add">
        <textarea id="note_content_add" class="class="form-control" style="width:100%; resize:vertical; max-height:1000px; min-height:200px;" required></textarea>
    </div>
    <button type="button" onclick="noteAdd()" class="btn btn-primary" style="margin-top:5px;">Save</button>';
?>