<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../../index.php?login');

if($_POST['error_comment'] == ""){
	echo '<div class="alert alert-danger">Insert Name.</div>';
	exit;
}
if(!isset($_POST['error_direcrory_selector'])){
	echo '<div class="alert alert-danger">Select Error Direcrory.</div>';
	exit;
}
if($_POST['error_direcrory_selector'] == 0 && $_POST['error_directory_hand'] == ""){
	echo '<div class="alert alert-danger">Insert "other page" field.</div>';
	exit;
}

if($_POST['error_direcrory_selector'] == 1){
	$errors = ORM::forTable('error_codes')->create();
	$errors->set(array(
	    'file' => $_POST['current_dir'],
	    'comment' => $_POST['error_comment'],
	    'creator' => $core->user->getData('id'),
	    'add_date' => time()
	));
	$errors->save();
	echo '
	<div class="alert alert-success">Error added to database</div>
	<p>PHP code:</p>
	<input class="form-control" onClick="this.select();" value="&lt;a href=&quot;&#039.$core->settings->get(&#039master_url&#039).&#039mail.php?error_code='.$errors->id().'&quot;&gt;Report this problem&lt;/a>" />

	';
}elseif($_POST['error_direcrory_selector'] == 0){
	$errors = ORM::forTable('error_codes')->create();
	$errors->set(array(
	    'file' => $_POST['error_directory_hand'],
	    'comment' => $_POST['error_comment'],
	    'creator' => $core->user->getData('id'),
	    'add_date' => time()
	));
	$errors->save();
	echo '
	<div class="alert alert-success">Error added to database</div>
	<p>PHP code:</p>
	<input class="form-control" onClick="this.select();" value="&lt;a href=&quot;&#039.$core->settings->get(&#039master_url&#039).&#039mail.php?error_code='.$errors->id().'&quot;&gt;Report this problem&lt;/a>" />

	';
}
?>