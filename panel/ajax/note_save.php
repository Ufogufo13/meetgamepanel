<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../../index.php?login');

if($_POST['note_name'] == ""){
	echo '<div class="alert alert-danger">Inser Name</div>';
	exit;
}
if(isset($_POST['note_id'])){
	$notes = ORM::forTable('notes')->findOne($_POST['note_id']);
	$notes->content = $_POST['note_content'];
	$notes->name = $_POST['note_name'];
	$notes->last_update = time();
	$notes->save();
	echo '<div class="alert alert-success">Note updated</div>';
}else{
	$note = ORM::forTable('notes')->create();
	$note->set(array(
	    'name' => $_POST['note_name'],
	    'owner' => $core->user->getData('id'),
	    'last_update' => time(),
	    'content' => $_POST['note_content']
	));
	$note->save();
	echo '<div class="alert alert-success">Note Created</div>';
}

?>