<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true){

	/*
	 * Set Language in Database (& Set Cookie)
	 */
	if(file_exists(SRC_DIR.'lang/'.$_GET['language'].'.json')){

		$account = ORM::forTable('users')->findOne($core->user->getData('id'));
        $account->set(array(
            'language' => $_GET['language']
        ));
        $account->save();

		setcookie("pp_language", $_GET['language'], time() + 2678400, '/');

	}

}else{

	/*
	 * Set Language Cookie for One Month
	 */
	if(file_exists(SRC_DIR.'lang/'.$_GET['language'].'.json'))
		setcookie("pp_language", $_GET['language'], time() + 2678400, '/');

}

/*
 * Redirect
 */
if(!isset($_SERVER["HTTP_REFERER"]) || $_SERVER["HTTP_REFERER"] == "")
	Components\Page::redirect('../../index.php?login');
else
	Components\Page::redirect($_SERVER["HTTP_REFERER"]);

?>
