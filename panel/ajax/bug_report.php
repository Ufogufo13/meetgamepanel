<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../../index.php?login');

require_once("../../src/captcha/recaptchalib.php");

$resp = recaptcha_check_answer($core->settings->get('captcha_priv'), $_SERVER["REMOTE_ADDR"], @$_POST["recaptcha_challenge_field"], @$_POST["recaptcha_response_field"]);

if($resp->is_valid){
	if(!isset($_POST['error_title'], $_POST['error_selector'], $_POST['content']))
		Components\Page::redirect('../mail.php?disp=missing_args&error=na');
	if(!preg_match('/^[\w-]{4,35}$/', $_POST['error_title'])) 
		Components\Page::redirect('../mail.php?id=1&error=error_title&disp=t_fail');
	
	if(isset($_POST['error_code'])){
		$error = ORM::forTable('error_codes')->findOne($_POST['error_code']);
	}else{
		$_POST['error_code']="NULL";
	}

	if($_POST['error_selector'] == "error"){
		$email = $core->settings->get('error_email');
		$error_sub_tittle = "[E]";
	}
	if($_POST['error_selector'] == "idea"){
		$email = $core->settings->get('idea_email');
		$error_sub_tittle = "[I]";
	}
	if($_POST['error_selector'] == "payments"){
		$email = $core->settings->get('payments_email');
		$error_sub_tittle = "[P]";
	}
	if($_POST['error_selector'] == "other"){
		$email = $core->settings->get('other_email');
		$error_sub_tittle = "[O]";
	}
	
	$core->email->buildEmail('bug_report', array(
	    'CONTENT' => $_POST['content'],
	    'TITTLE' => $_POST['error_title'],
	    'USERNAME' => $core->user->getData('username'),
	    'USEREMAIL' => $core->user->getData('email'),
	    'ERRORCODE' => $_POST['error_code'],
	    'ERRORINFILE' => $error['file'],
	    'ERRORCOMMENT' => $error['comment']
	))->dispatch($email, $error_sub_tittle.$_POST['error_title'].' -'.$core->user->getData('username'));

	Components\Page::redirect('../mail.php?disp=e_sent&error=na');
}else{
	setcookie("textcontainer", $_POST['content'], time()+30, "/");
	setcookie("textcontainer_tittle", $_POST['error_title'], time()+30, "/");
	Components\Page::redirect('../mail.php?disp=captcha&error=na');
}


?>