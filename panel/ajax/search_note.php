<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['owner_id']))
    echo "Post Error. Refresh"; 

echo '<script language="JavaScript" src="'.$core->settings->get('assets_url').'javascript/notesave.js" type="text/javascript"></script>'; 

$notes = ORM::forTable('notes')->select('name')->select('last_update')->select('id')
            ->where('owner', $_POST['owner_id'])
            ->findArray();

$notes = array_reverse($notes);

$returnRows = '';
foreach($notes as &$row){
    $returnRows .= '
    <tr>
        <td><a href="#" name="note_id" id="'.$row['id'].'" style="text-decoration:none;">'.$row['name'].'</a></td>
        <td>'.gmdate("D, d/M/Y H:i:s", $row['last_update']).'</td>
    </tr>
    ';
}

if($notes==NULL){
    echo '<div class="alert alert-info">You do not currently have any notes.</div>';
}else{
    echo '
        <table class="table table-striped table-bordered table-hover" style="margin-top:20px;">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Last Update</th>
                </tr>
            </thead>
            <tbody>
                '.$returnRows.'
            </tbody>
        </table>
    ';
}

?>