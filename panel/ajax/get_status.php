<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true){

	if(isset($_POST['server'])){

		/*
		 * Query Database
		 */
		$status = ORM::forTable('servers')->select('servers.gsd_id')->select('nodes.ip')->select('nodes.gsd_secret')->select('nodes.gsd_listen')
					->join('nodes', array('servers.node', '=', 'nodes.id'))
					->where('servers.id', $_POST['server'])
					->findOne();

		if($core->gsd->check_status($status->ip, $status->gsd_listen, $status->gsd_id, $status->gsd_secret) === false)
			exit('#E33200');
		else
			exit('#53B30C');

	}else
		exit('#FF9900');

}else
	exit('#FF9900');
?>