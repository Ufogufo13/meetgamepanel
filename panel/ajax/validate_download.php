<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
header('Content-Type: application/json');
require_once '../../src/core/core.php';

/*
* Illegally Accessed File
*/
if(!isset($_POST['token'], $_POST['server'])) {

	http_response_code(403);
	exit();

}

/*
* Verify Identity
*/
$download = ORM::forTable('downloads')
	->where(array(
		'server' => $_POST['server'],
		'token' => $_POST['token']
	))->findOne();

if(!$download) {

	http_response_code(404);
	exit();

} else {

	$download->delete();

	http_response_code(200);
	exit(json_encode(array(
		'path' => $download->path
	)));

}