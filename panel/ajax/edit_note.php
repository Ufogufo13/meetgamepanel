<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['note_id']))
    echo "Post Error. Refresh"; 

echo '<script language="JavaScript" src="'.$core->settings->get('assets_url').'javascript/notesave.js" type="text/javascript"></script>';

$note = ORM::forTable('notes')->findOne($_POST['note_id']);

if($note==NULL){
    echo '<div class="alert alert-info">Search ERROR. <a href="'.$core->settings->get('master_url').'mail.php?error_code=1123123'.'">Report this problem</a></div>';
}else{
    echo '
    <div class="form-group col-4 nopad">
        <label for="note_name" class="control-label">Name:</label>
        <div>
            <input type="text" autocomplete="off" name="note_name" class="form-control" value="'.$note['name'].'" id="note_names"/>
        </div>
    </div>
    <div name="note_content">
        <textarea id="note_content" value="'.$note['id'].'" class="class="form-control" style="width:100%; resize:vertical; max-height:1000px; min-height:200px;">'.$note['content'].'</textarea>
    </div>
    <button type="button" onclick="noteSave()" class="btn btn-primary" style="margin-top:5px;">Save</button>
    <button type="button" onclick="noteDelete()" class="btn btn-primary" style="margin-top:5px; float:right;">Delete</button>';
}