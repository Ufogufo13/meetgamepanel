<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
header('Content-Type: application/json');
require_once '../../src/core/core.php';

/*
 * Illegally Accessed File
 */
if(!isset($_POST['username'], $_POST['password'])) {

	http_response_code(403);
	exit();

}

if(!preg_match('^([mc-]{3})([\w\d\-]{12})[\-]([\d]+)$^', $_POST['username'], $matches)) {

	http_response_code(403);
	exit();

} else {

	$username = $matches[1].$matches[2];
	$serverid = $matches[3];

}

/*
 * Verify Identity
 */
$server = ORM::forTable('servers')
			->selectMany('encryption_iv', 'ftp_pass', 'gsd_secret')
			->where(array('gsd_id' => $serverid, 'ftp_user' => $username))
			->findOne();

	if(!$server) {

		http_response_code(403);
		exit();

	} else {

		if($core->auth->encrypt($_POST['password'], $server->encryption_iv) != $server->ftp_pass) {

			http_response_code(403);
			exit();

		} else {

			http_response_code(200);
			die(json_encode(array('authkey' => $server->gsd_secret)));

		}

	}
?>
