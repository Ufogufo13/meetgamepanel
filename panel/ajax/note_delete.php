<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../../index.php?login');

$notes = ORM::forTable('notes')->findOne($_POST['note_id']);
$notes->delete();

echo '<div class="alert alert-success">Note Deleted</div>';
?>