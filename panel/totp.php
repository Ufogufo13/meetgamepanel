<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../src/core/core.php');
$error = '';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token')) !== true)
	Components\Page::redirect('index.php?login');

/*
 * Display Page
 */
echo $twig->render(
		'panel/totp.html', array(
			'totp' => array(
				'enabled' => $core->user->getData('use_totp')
			),
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));

?>
