<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['network_id']))
    echo "Error. Refresh"; 

echo '<script language="JavaScript" src="'.$core->settings->get('assets_url').'javascript/getgametypestatus.js" type="text/javascript"></script>';

$srvgametypess = ORM::forTable('mc_gametypes')->select('mc_gametypes.*')
                ->where('network_id', $_POST['network_id'])
                ->findArray();

$returnRows = '';
foreach ($srvgametypess as &$srvgametypess) {
    $status = ORM::forTable('servers')->select('servers.gsd_id')->select('nodes.ip')->select('nodes.gsd_secret')->select('nodes.gsd_listen')
        ->join('nodes', array('servers.node', '=', 'nodes.id'))
        ->where(array('servers.network_id' => $_POST['network_id'],
            'servers.gametype_id' => $srvgametypess['id']
            ))
        ->findMany();
    foreach ($status as &$status) {
        if($core->gsd->check_status($status->ip, $status->gsd_listen, $status->gsd_id, $status->gsd_secret) === false){
            $serversoffline[$kej]++;
        }else{
            $serversonline[$kej]++;
        }
    }
    if(!isset($serversonline[$kej]))
        $serversonline[$kej] = 0;
    if(!isset($serversoffline[$kej]))
        $serversoffline[$kej] = 0;

    $returnRows .= '
        <tr>
            <td><a href="#">'.$srvgametypess['name'].'</a><a class="fa fa-arrow-circle-o-right" name="gametype_id" id="'.$_POST['network_id'].'" value="'.$srvgametypess['id'].'" style="font-size:16px; padding-left:8px; text-decoration:none;"></a></td>
            <td>'.$serversonline[$kej].'</td>
            <td>'.$serversoffline[$kej].'</td>
        </tr>
    ';
    $kej++;
}

if($srvgametypess==NULL){
    echo '<div class="alert alert-info">You do not currently have any gametypes in your network.</div>';
}else{
    echo '
        <h4>Summary for Network</h4></hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Gametype</th>
                    <th>Online</th>
                    <th>Offline</th>
                </tr>
            </thead>
            <tbody>
                '.$returnRows.'
            </tbody>
        </table>
    ';
}

?>