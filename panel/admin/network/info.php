<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../index.php?login');

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

echo $twig->render(
        'admin/network/info.html', array(
            'id_network' => $networkId['network_id'],
            'footer' => array(
                'seconds' => number_format((microtime(true) - $pageStartTime), 4)
            )
        ));
?>
