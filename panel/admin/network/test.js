$("[name='gametype_id']").click(function(){
    var networkIds = $(this).attr("value");
    var Gunwix = $(this).attr("id");
    $.ajax({
        type: "POST",
        url: "ajax/summary_gametype.php",
        data: { method: "simple", gametype_id: networkIds, network_id: Gunwix },
        success: function(data) {
            $("#search_results").slideUp(function(){
                $("#search_results").html();
                $("#search_results").html(data);
                $("#search_results").fadeIn(function(){
                    $("#ss_active_spin").html('&rarr;');
                    return false;
                });
            });
        }
    });
});