<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../index.php?login');

$head = trim(file_get_contents('../../.git/HEAD'));
if(is_array(explode('/', $head))){
	list($ignore, $path) =  explode(" ", $head);
	$git = '('.$head.') (sha: '.substr(trim(file_get_contents('../../.git/'.$path)), 0 ,8).')';
}else
	$git = $head;

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$srvgametypess = ORM::forTable('mc_gametypes')->select('mc_gametypes.*')
                ->where('network_id', $networkId['network_id'])
                ->findArray();

$servers = ORM::forTable('servers')->select('servers.*')->where('network_id', $networkId['network_id'])
            ->findMany();

foreach ($srvgametypess as &$srvgametypess) {
    $status = ORM::forTable('servers')->select('servers.gsd_id')->select('nodes.ip')->select('nodes.gsd_secret')->select('nodes.gsd_listen')
        ->join('nodes', array('servers.node', '=', 'nodes.id'))
        ->where(array('servers.network_id' => $networkId['network_id'],
            'servers.gametype_id' => $srvgametypess['id']
            ))
        ->findMany();
    foreach ($status as &$status) {
        if($core->gsd->check_status($status->ip, $status->gsd_listen, $status->gsd_id, $status->gsd_secret) === false){
            $serversoffline[$kej]++;
        }else{
            $serversonline[$kej]++;
        }
    }
    if(!isset($serversonline[$kej]))
        $serversonline[$kej] = 0;
    if(!isset($serversoffline[$kej]))
        $serversoffline[$kej] = 0;
    $gejmtyp[$kej] = array(
            "id" => $srvgametypess['id'],
            "networkid" => $srvgametypess['network_id'],
            "name" => $srvgametypess['name'],
            "online" => $serversonline[$kej],
            "offline" => $serversoffline[$kej]
        );
    $kej++;
}
echo $twig->render(
        'admin/network/info.html', array(
            'srvgametype' => $gejmtyp,
            'footer' => array(
                'seconds' => number_format((microtime(true) - $pageStartTime), 4)
            )
        ));
?>
