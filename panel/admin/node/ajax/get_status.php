<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM;

require_once '../../../../src/core/core.php';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true)) {

	if(isset($_POST['ip'])){

		/*
		 * Query Servers
		 */
		list($ip, $port) = explode('+', $_POST['ip']);

		if(!$core->gsd->avaliable($ip, $port)) {
			exit('#E33200');
		} else {
			exit('#53B30C');
		}

	} else {

		exit('#FF9900');

	}

} else {

	exit('#FF9900');

}