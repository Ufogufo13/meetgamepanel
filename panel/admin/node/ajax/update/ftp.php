<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');


if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php?login');
}

if(isset($_GET['do']) && $_GET['do'] == 'ipuser') {

	if(!isset($_POST['nid']) || !is_numeric($_POST['nid']))
		Components\Page::redirect('../../list.php');

	if(!filter_var($_POST['ip'] , FILTER_VALIDATE_IP))
		Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&error=ip&disp=ip_fail&tab=ftp');

	/*
	 * Run Update on Node Table
	 */
	$node = ORM::forTable('nodes')->findOne($_POST['nid']);
	$node->ip = $_POST['ip'];
	$node->save();

	Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&tab=ftp');

}
?>