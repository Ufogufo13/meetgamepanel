<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php?login');
}

if(!isset($_POST['nid']) || !is_numeric($_POST['nid']))
	Components\Page::redirect('../../list.php');

if(!isset($_POST['name'], $_POST['fqdn']))
	Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&disp=missing_args');

/*
 * Validate Node Name
 */
if(!preg_match('/^[\w.-]{1,15}$/', $_POST['name']))
	Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&error=name&disp=n_fail');

/*
 * Validate IP
 */
if(!filter_var(gethostbyname($_POST['fqdn']), FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE))
	Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&error=fqdn&disp=ip_fail');

/*
 * Update Record
 */
$node = ORM::forTable('nodes')->findOne($_POST['nid']);
$node->node = $_POST['name'];
$node->fqdn = $_POST['fqdn'];
$node->save();

Components\Page::redirect('../../view.php?id='.$_POST['nid']);

?>