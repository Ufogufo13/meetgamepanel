<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
    Components\Page::redirect('../../index.php?login');

$gamevers = ORM::forTable('mc_gameversion')->select('name')->findMany();

$networkId = ORM::forTable('users')->select('network_id')
	->where('users.id', $core->user->getData('id'))
	->findOne();

$gametypes = ORM::forTable('mc_gametypes')->selectMany('id', 'name')->where('network_id', $networkId->network_id)->findMany();
$gametypes = array_reverse($gametypes);


echo $twig->render(
    'admin/server/groupmanage.html', array(
        'engine' => $engines,
        'gametype' => $gametypes,
        'footer' => array(
            'seconds' => number_format((microtime(true) - $pageStartTime), 4)
        )
    ));
?>