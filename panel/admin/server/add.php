<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

if(isset($_GET['do']) && $_GET['do'] == 'generate_password')
	exit($core->auth->keygen(12));

$locations = ORM::forTable('locations')->findMany();
$nodes = ORM::forTable('nodes')->selectMany('id', 'node')->findMany();

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();
    
$gametypes = ORM::forTable('mc_gametypes')->selectMany('id', 'name')->where('network_id', $networkId->network_id)->findMany();
$gametypes = array_reverse($gametypes);

$engines = ORM::forTable('mc_engines')->selectMany('id', 'file')->findMany();
$engines = array_reverse($engines);

echo $twig->render(
	'admin/server/add.html', array(
        'nodes' => $nodes,
        'engine' => $engines,
        'gametype' => $gametypes,
		'locations' => $locations,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>