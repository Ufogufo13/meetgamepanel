<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \Unirest;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../index.php');

if(!isset($_POST['sid']))
	Components\Page::redirect('../../find.php');

$core->server->rebuildData($_POST['sid']);
$core->user->rebuildData($core->server->getData('owner_id'));

/*
 * Validate Disk & Memory
 */
if(!is_numeric($_POST['alloc_mem']) || !is_numeric($_POST['cpu_limit']) || !is_numeric($_POST['alloc_mem_init']) || !is_numeric($_POST['alloc_disk'])) {
	Components\Page::redirect('../../view.php?id='.$_POST['sid'].'&error=alloc_mem|cpu_limit&disp=m_fail&tab=server_sett');
}

/*
 *	Validate Server Name
 */
if(!preg_match('/^[\w-]{4,35}$/', $_POST['server_name'])) {
	Components\Page::redirect('../../view.php?id=1&error=server_name&disp=n_fail&tab=server_sett');
}
$enginee = ORM::forTable('mc_engines')->selectMany('path', 'file', 'controlsum')->where('id', $_POST['jarfile'])->findOne();
$enginee = array('path' => $enginee->path,
				 'controlsum' => $enginee->controlsum,
				 'name' => $enginee->file);

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$networkk = ORM::forTable('mc_networks')->select('name')->where('id', $networkId->network_id)->findOne();

$server = ORM::forTable('servers')->findOne($core->server->getData('id'));
$server->name = $_POST['server_name'];
$server->max_ram = $_POST['alloc_mem'];
$server->init_ram = $_POST['alloc_mem_init'];
$server->disk_space = $_POST['alloc_disk'];
$server->server_jar = $networkk->name."_".$_POST['server_name'].".jar";
$server->engine_id = $_POST['jarfile'];
$server->cpu_limit = $_POST['cpu_limit'];
$server->threads_limit = $_POST['threads'];
$server->active = $_POST['active'];
$server->save();


/*
 * Build the Data
 */
try {

	$request = Unirest::put(
		"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id'),
		array(
			"X-Access-Token" => $core->server->nodeData('gsd_secret')
		),
		array(
			"variables" => json_encode(array(
				"-jar" =>  $networkk->name."_".$_POST['server_name'].".jar",
				"-Xmx" => $_POST['alloc_mem']."M",
				"-Xms" => $_POST['alloc_mem_init']."M",
				"-XX:ParallelGCThreads=" => $_POST['threads']
			)),
			"build" => json_encode(array(
				"cpu" => (int) $_POST['cpu_limit'],
				"controlsum" => $enginee['controlsum'],
				"engine_path" => $enginee['path']
			))
		)
	);

} catch(\Exception $e) {
	Components\Page::redirect('../../view.php?id='.$_POST['sid'].'&disp=o_fail&tab=server_sett');
}

Components\Page::redirect('../../view.php?id='.$_POST['sid'].'&tab=server_sett');
?>
