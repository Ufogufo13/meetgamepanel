<?php

namespace PufferPanel\Core;
use \ORM, \Unirest, \Tracy;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true) {

	if(!$core->user->hasPermission('console.power')) {
		exit('You do not have the required permissions to perform this function.');
	}

	$networkId = ORM::forTable('users')->select('network_id')
		->where('users.id', $core->user->getData('id'))
		->findOne();

	$forstart = ORM::forTable('servers')->where(array(
	    'network_id' => $networkId->network_id,
	    'gametype_id' => $_POST['gametype_name']
	))->findMany();
	$totalservers = count($forstart);
	$rewrite = false;
	$errorMessage = "Unable to process your request. Please try again.";

	foreach ($forstart as &$forstart) {

		$nodedataa = ORM::forTable('nodes')->find_one($forstart['node']);

		try {

			$response = Unirest::get(
				"http://".$nodedataa['ip'].":".$nodedataa['gsd_listen']."/gameservers/".$forstart['gsd_id']."/file/server.properties",
				array(
					"X-Access-Token" => $forstart['gsd_secret']
				)
			);

		} catch(\Exception $e) {

			Tracy\Debugger::log($e);
			exit($errorMessage." Unable to connect to remote host. ".$forstart['name']);

		}

		/*
		 * Typically Means Server is Off
		 */
		if(!in_array($response->code, array(200, 500))) {
			switch($response->code) {

				case 403:
					exit($errorMessage." Authentication error encountered. ".$forstart['name']);
					break;
				default:
					exit("$errorMessage HTTP/$response->code. Invalid response was recieved. ($response->raw_body)");
					break;

			}
		}

		if($response->code == 500 || !isset($response->body->contents) || empty($response->body->contents)) {

			/*
			 * Create server.properties
			 */
			if(!file_exists(APP_DIR.'templates/server.properties.tpl') || empty(file_get_contents(APP_DIR.'templates/server.properties.tpl')))
				exit($errorMessage." No Template Avaliable for server.properties");

			$put = Unirest::put(
				"http://".$nodedataa['ip'].":".$nodedataa['gsd_listen']."/gameservers/".$forstart['gsd_id']."/file/server.properties",
				array(
					"X-Access-Token" => $forstart['gsd_secret']
				),
				array(
					"contents" => sprintf(file_get_contents(APP_DIR.'templates/server.properties.tpl'), $forstart['server_port'], $forstart['server_ip'])
				)
			);

	        if(!empty($put->body)) {
	        	exit($errorMessage." Unable to make server.properties for ".$forstart['name']);
			}

			$core->log->getUrl()->addLog(0, 1, array('system.create_serverprops', 'A new server.properties file was created for your server.'));

		}

	    /*
		 * Connect and Run Function
		 */
		$get = Unirest::get(
			"http://".$nodedataa['ip'].":".$nodedataa['gsd_listen']."/gameservers/".$forstart['gsd_id']."/on",
			array(
				"X-Access-Token" => $forstart['gsd_secret']
			)
		);

		if($get->body != "ok") {
			exit($errorMessage." Unable to start ".$forstart['name']." server (".$get->body->message.")");
		}

		echo $forstart['name']." started </br>";
		sleep(1);
	}

} else {

	die('Invalid Authentication.');

}

Components\Page::redirect('../../../index.php');
?>

