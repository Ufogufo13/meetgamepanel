<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

if(isset($_GET['do']) && $_GET['do'] == 'generate_password')
	exit($core->auth->keygen(rand(12, 18)));

if(!isset($_GET['id']))
	Components\Page::redirect('find.php?error=no_id');

$core->server->rebuildData($_GET['id']);
$core->user->rebuildData($core->server->getData('owner_id'));

if(!$core->server->getData('hash') || $core->server->getData('hash') === false)
	Components\Page::redirect('find.php?error=invalid_id');

$enginess = ORM::forTable('mc_engines')->selectMany('id', 'file')->findMany();
$enginess = array_reverse($enginess);

$networkId = ORM::forTable('users')->select('network_id')
	->where('users.id', $core->user->getData('id'))
	->findOne();

$networkName = ORM::forTable('mc_networks')->select('name')
	->where('id', $networkId['network_id'])
	->findOne();

echo $twig->render('admin/server/view.html', array(
		'node' => $core->server->nodeData(),
		'decoded' => array('ips' => json_decode($core->server->nodeData('ips'), true), 'ports' => json_decode($core->server->nodeData('ports'), true)),
		'server' => $core->server->getData(),
		'engine' => $enginess,
		'networkname' => $networkName,
		'user' => $core->user->getData(),
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>