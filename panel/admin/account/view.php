<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../index.php?login');
}

if(isset($_GET['do']) && $_GET['do'] == 'generate_password')
	exit($core->auth->keygen(rand(12,18)));

if(!isset($_GET['id']))
	Components\Page::redirect('find.php');

/*
 * Select User Information
 */
$core->user->rebuildData($_GET['id']);

if(!$core->user->getData('id') || $core->user->getData('id') === false)
	Components\Page::redirect('find.php?error=no_user');

$date1 = new \DateTime(date('Y-m-d', $core->user->getData('register_time')));
$date2 = new \DateTime(date('Y-m-d', time()));

$user = $core->user->getData();
$user['register_time'] = date('F j, Y g:ia', $core->user->getData('register_time')).' ('.$date2->diff($date1)->format("%a Days Ago").')';

$networkId = ORM::forTable('users')->select('network_id')
	->where('users.id', $core->user->getData('id'))
	->findOne();

$networkName = ORM::forTable('mc_networks')->select('name')
	->where('id', $networkId['network_id'])
	->findOne();
/*
 * Select Servers Owned by the User
 */
$servers = ORM::forTable('servers')->select('servers.*')->select('nodes.node', 'node_name')
			->join('nodes', array('servers.node', '=', 'nodes.id'))
			->where(array('servers.owner_id' => $core->user->getData('id'), 'servers.active' => 1))
			->findArray();

echo $twig->render(
		'admin/account/view.html', array(
			'user' => $user,
			'servers' => $servers,
			'networkname' => $networkName,
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
		));
?>