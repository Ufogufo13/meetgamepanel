<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../index.php?login');

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$srvgametypess = ORM::forTable('mc_gametypes')->select('mc_gametypes.*')
                ->where('network_id', $networkId['network_id'])
                ->findArray();

//RAM ALLOCATED
$ramservers = ORM::forTable('servers')->where('network_id', $networkId['network_id'])->sum('max_ram');
$maxramservers = ORM::forTable('mc_networks')->where('id', $networkId['network_id'])->sum('max_ram');
$usedram = $maxramservers - $ramservers;
//DISK ALLOCATED
$diskservers = ORM::forTable('servers')->where('network_id', $networkId['network_id'])->sum('disk_space');
$maxdiskservers = ORM::forTable('mc_networks')->where('id', $networkId['network_id'])->sum('max_disk');
$useddisk = $maxdiskservers - $diskservers;
//ONLINE/OFFLINE
foreach ($srvgametypess as &$srvgametypess) {
    $status = ORM::forTable('servers')->select('servers.gsd_id')->select('nodes.ip')->select('nodes.gsd_secret')->select('nodes.gsd_listen')
        ->join('nodes', array('servers.node', '=', 'nodes.id'))
        ->where(array('servers.network_id' => $networkId['network_id'],
            'servers.gametype_id' => $srvgametypess['id']
            ))
        ->findMany();
    foreach ($status as &$status) {
        if($core->gsd->check_status($status->ip, $status->gsd_listen, $status->gsd_id, $status->gsd_secret) === false){
            $serversoffline[$kej]++;
        }else{
            $serversonline[$kej]++;
        }
    }
    if(!isset($serversonline[$kej]))
        $serversonline[$kej] = 0;
    if(!isset($serversoffline[$kej]))
        $serversoffline[$kej] = 0;
    $gejmtyp = array(
            "online" => $gejmtyp['online'] + $serversonline[$kej],
            "offline" => $gejmtyp['offline'] + $serversoffline[$kej]
        );
    $kej++;
}

echo $twig->render(
        'admin/index.html', array(
            'ramservers' => $ramservers,
            'maxramservers' => $maxramservers,
            'usedram' => $usedram,
            'diskservers' => $diskservers,
            'maxdiskservers' => $maxdiskservers,
            'useddisk' => $useddisk,
            'srvgametype' => $gejmtyp,
            'footer' => array(
                'seconds' => number_format((microtime(true) - $pageStartTime), 4)
            )
        ));
?>
