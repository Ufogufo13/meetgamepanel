<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['gametype_id']))
    echo "Error. Refresh"; 

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$servers = ORM::forTable('servers')
            ->select('servers.id')->select('servers.active')->select('servers.name')
            ->where(array('servers.network_id' => $networkId['network_id'],
                'servers.gametype_id' => $_POST['gametype_id']
            ))
            ->orderByDesc('servers.active')
            ->findArray();
$returnRows = '';
foreach($servers as &$row){
    $isactive = ($row['active'] == 1) ? '<a href="../admin/server/view.php?id='.$row['id'].'">'.$row['name'].'</a>' : '<a href="../admin/server/view.php?id='.$row['id'].'">'.$row['name'].'</a><span class="label label-danger" style="float:right;">Disabled</span>';
    $returnRows .= '
    <tr class="dynUpdate" id="'.$row['id'].'">
        <td>'.$isactive.'</td>
        <td style="width:26px;"><i id="applyUpdate" class="fa fa-circle-o-notch fa-spinner fa-spin"></i></td>
    </tr>
    ';
}
echo $servers;
if($servers==NULL){
    echo "BRAK SERWEROW";
}else{
    echo '
        <h4>Summary for Gametype</h4></hr>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                '.$returnRows.'
            </tbody>
        </table>
    ';
}

?>