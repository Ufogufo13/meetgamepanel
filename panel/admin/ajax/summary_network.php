<?php
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['network_name']))
    Components\Page::redirect('../index.php'); 

$gametypes = ORM::forTable('mc_gametypes')->selectMany('id', 'name')->findMany();

$networknames = ORM::forTable('mc_networks')->selectMany('id', 'name')->findMany();

$srvgametypess = ORM::forTable('mc_gametypes')->select('mc_gametypes.*')
                ->where('network_id', $_POST['network_name'])
                ->findArray();

$servers = ORM::forTable('servers')->select('servers.*')->where('network_id', $_POST['network_name'])
            ->findMany();

foreach ($srvgametypess as &$srvgametypess) {
    $status = ORM::forTable('servers')->select('servers.gsd_id')->select('nodes.ip')->select('nodes.gsd_secret')->select('nodes.gsd_listen')
        ->join('nodes', array('servers.node', '=', 'nodes.id'))
        ->where(array('servers.network_id' => $_POST['network_name'],
            'servers.gametype_id' => $srvgametypess['id']
            ))
        ->findMany();
    foreach ($status as &$status) {
        if($core->gsd->check_status($status->ip, $status->gsd_listen, $status->gsd_id, $status->gsd_secret) === false){
            $serversoffline[$kej]++;
        }else{
            $serversonline[$kej]++;
        }
    }
    if(!isset($serversonline[$kej]))
        $serversonline[$kej] = 0;
    if(!isset($serversoffline[$kej]))
        $serversoffline[$kej] = 0;
    $gejmtyp[$kej] = array(
            "id" => $srvgametypess['id'],
            "networkid" => $srvgametypess['network_id'],
            "name" => $srvgametypess['name'],
            "online" => $serversonline[$kej],
            "offline" => $serversoffline[$kej]
        );
    $kej++;
}


echo $twig->render(
        'admin/summary_network.html', array(
            'networkname' => $networknames,
            'gametype' => $gametypes,
            'servers' => $servers,
            'srvgametype' => $gejmtyp,
            'footer' => array(
                'seconds' => number_format((microtime(true) - $pageStartTime), 4)
            )
        ));
?>