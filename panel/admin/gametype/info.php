<?php

namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$networkk = ORM::forTable('mc_networks')->select('name')->where('id', $networkId->network_id)->findOne();

$gametypes = ORM::forTable('mc_gametypes')->select('id')->select('name')
	->where('network_id', $networkId->network_id)
    ->findArray();

$kej=0;
foreach ($gametypes as &$row) {
    $counter = ORM::forTable('servers')->select('id')->where('gametype_id', $row['id'])->findArray();
    $gejmtyp[$kej] = array(
    		"id" => $row['id'],
            "name" => $row['name'],
            "count" => count($counter)
        );
    $kej++;
}


echo $twig->render(
	'admin/gametype/info.html', array(
		'networkname' => $networkk,
		'srvgametype' => $gejmtyp,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>