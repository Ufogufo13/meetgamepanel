<?php

namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$networkk = ORM::forTable('mc_networks')->select('name')->where('id', $networkId->network_id)->findOne();

$gametypes = ORM::forTable('mc_gametypes')->select('id')->select('name')
	->where('network_id', $networkId->network_id)
    ->findMany();

echo $twig->render(
	'admin/gametype/addgametype.html', array(
		'networkname' => $networkk,
		'gametypes' => $gametypes,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>