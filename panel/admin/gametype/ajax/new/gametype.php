<?php
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

//Cookies :3
setcookie("__TMP_pp_admin_addgametype", json_encode($_POST), time() + 30, '/', $core->settings->get('cookie_website'));

if(!isset($_POST['gametype_name']))
    Components\Page::redirect('../../info.php?error=n_fail');

if(!preg_match('/^[\w.-]{3,12}$/', $_POST['gametype_name']))
    Components\Page::redirect('../../info.php?error=m_fail');

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$ftpUsergroup = Components\Functions::generateFTPUsernamegroup($_POST['gametype_name']);
/*
* Update MySQL
*/
$gameversion = ORM::forTable('mc_gametypes')->create();
$gameversion->set(array(
    'name' => $_POST['gametype_name'],
    'gametype_user' => $ftpUsergroup,
    'network_id' => $networkId->network_id,
    'add_date' => time()
));
$gameversion->save();


Components\Page::redirect('../../info.php');
?>
