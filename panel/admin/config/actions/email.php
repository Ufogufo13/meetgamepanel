<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../index.php?login');

setcookie("__TMP_pp_admin_updateglobal", json_encode($_POST), time() + 30, '/');

if(!isset($_POST['smail_method'], $_POST['sendmail_email'], $_POST['postmark_api_key'], $_POST['mandrill_api_key'], $_POST['mailgun_api_key'], $_POST['sendgrid_api_key']))
	Components\Page::redirect('../email.php?error=smail_method|sendmail_email|postmark_api_key|mandrill_api_key|mailgun_api_key|sendgrid_api_key');

if(!in_array($_POST['smail_method'], array('php', 'postmark', 'mandrill', 'mailgun', 'sendgrid')))
	Components\Page::redirect('../email.php?error=smail_method');

if(!filter_var($_POST['sendmail_email'], FILTER_VALIDATE_EMAIL))
	Components\Page::redirect('../email.php?error=sendmail_email');

if($_POST['smail_method'] != 'php' && empty($_POST[$_POST['smail_method'].'_api_key']))
	Components\Page::redirect('../email.php?error=smail_method|'.$_POST['smail_method'].'_api_key');

/*
 * Handle Sendgrid Information
 */
$iv = $core->auth->generate_iv();
if(strpos($_POST['sendgrid_api_key'], '|') !== false)
	$_POST['sendgrid_api_key'] = $iv.'.'.$core->auth->encrypt($_POST['sendgrid_api_key'], $iv);

$query = ORM::forTable('acp_settings')->rawExecute("
UPDATE acp_settings SET setting_val = CASE setting_ref
	WHEN 'sendmail_method' THEN :sendmail_method
	WHEN 'sendmail_email' THEN :sendmail_email
	WHEN 'postmark_api_key' THEN :postmark_api_key
	WHEN 'mandrill_api_key' THEN :mandrill_api_key
	WHEN 'mailgun_api_key' THEN :mailgun_api_key
	WHEN 'sendgrid_api_key' THEN :sendgrid_api_key
	ELSE setting_val
END
", array(
	'sendmail_method' => $_POST['smail_method'],
	'sendmail_email' => $_POST['sendmail_email'],
	'postmark_api_key' => $_POST['postmark_api_key'],
	'mandrill_api_key' => $_POST['mandrill_api_key'],
	'mailgun_api_key' => $_POST['mailgun_api_key'],
	'sendgrid_api_key' => $_POST['sendgrid_api_key'],
));

Components\Page::redirect('../email.php?success=true');

?>