<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../../index.php');

setcookie("__TMP_pp_admin_updateglobal", json_encode($_POST), time() + 30, '/');

if(!isset($_POST['main_url'], $_POST['master_url'], $_POST['assets_url']))
	Components\Page::redirect('../urls.php?error=main_url|master_url|assets_url');

foreach($_POST as $id => $val)
	if(!preg_match('/^((https?:){0,1})(\/\/){1}([-\d\w\/.]*)$/', $val))
		Components\Page::redirect('../urls.php?error='.$id);
	else
		$_POST[$id] = preg_replace('/^(http:|https:)(.*)?$/', '$2', $val);

$query = ORM::forTable('acp_settings')->rawExecute("
UPDATE acp_settings SET setting_val = CASE setting_ref
	WHEN 'main_website' THEN :main_url
	WHEN 'master_url' THEN :master_url
	WHEN 'assets_url' THEN :assets_url
	ELSE setting_val
END
", array(
	'main_url' => $_POST['main_url'],
	'master_url' => $_POST['master_url'],
	'assets_url' => $_POST['assets_url']
));

Components\Page::redirect('../urls.php?success=true');

?>