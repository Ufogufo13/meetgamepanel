<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
    Components\Page::redirect('../../../index.php?login');

if(!isset($_POST['permissions']))
    Components\Page::redirect('../global.php?error=general_setting');

$query = ORM::forTable('acp_settings')->rawExecute("
UPDATE acp_settings SET setting_val = CASE setting_ref
    WHEN 'use_api' THEN :enable_api
    WHEN 'force_online' THEN :force_online
    WHEN 'https' THEN :https
    WHEN 'allow_subusers' THEN :allow_subusers
    ELSE setting_val
END
", array(
    'enable_api' => (!in_array('use_api', $_POST['permissions'])) ? 0 : 1,
    'force_online' => (!in_array('force_online', $_POST['permissions'])) ? 0 : 1,
    'https' => (!in_array('https', $_POST['permissions'])) ? 0 : 1,
    'allow_subusers' => (!in_array('allow_subusers', $_POST['permissions'])) ? 0 : 1
));

Components\Page::redirect('../global.php?success=true');

?>