<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;
require_once('../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token')) !== true){
	Components\Page::redirect('index.php?login');
	exit();
}
if(isset($_COOKIE['textcontainer'])){
	$textcontainer=$_COOKIE['textcontainer'];
}else{
	$textcontainer="";
}

if(isset($_COOKIE['textcontainer_tittle'])){
	$textcontainer_tittle=$_COOKIE['textcontainer_tittle'];
}else{
	$textcontainer_tittle="";
}

if(!isset($_GET['error_code']))
	$_GET['error_code']="NULL";


echo $twig->render(
		'panel/mail.html', array(
			'text_editor' => $textcontainer,
			'text_editor_tittle' => $textcontainer_tittle,
			'error_codek' => $_GET['error_code'],
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));
?>
