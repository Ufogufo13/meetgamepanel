<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

if(!isset($_POST['location']))
	exit('No location was Defined');

$nodes = ORM::forTable('nodes')->where('location', $_POST['location'])->findMany();

?>

<div class="form-group col-6 nopad-right">
	<label for="server_name" class="control-label">Location Node</label>
	<div>
		<select name="node" id="getNode" class="form-control">
        <?php

			if($nodes) {

				foreach($nodes as &$node) {

					echo '<option value="'.$node->id.'"> '.$node->node.' '.(($node->public == 0) ? '[Private]' : null).'</option>';

				}

			}

		?>
    	</select>
	</div>
</div>
<?php
if(!$nodes) {
	echo "<script type=\"text/javascript\">$('#noNodes').show();</script>";
}
?>
