<?php
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

//Cookies :3
setcookie("__TMP_pp_admin_addengine", json_encode($_POST), time() + 30, '/', $core->settings->get('cookie_website'));

if(!isset($_POST['network_name']))
    Components\Page::redirect('../../addengine.php?disp=missing_args&error=na');

/*
* Update MySQL
*/
$gameversion = ORM::forTable('mc_networks')->create();
$gameversion->set(array(
    'name' => $_POST['network_name'],
    'add_date' => time()
));
$gameversion->save();


Components\Page::redirect('../../view.php');
?>
