<?php
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

//Cookies :3
setcookie("__TMP_pp_admin_removeengine", json_encode($_POST), time() + 30, '/', $core->settings->get('cookie_website'));

if(!isset($_POST['engine_id']))
    Components\Page::redirect('../../engines.php?disp=missing_args&error=na');

/*
* remove from folder
*/
$pathtodelete = ORM::forTable('mc_engines')->select('path')->where('id', $_POST['engine_id'])->findOne();
$pathtodelete = array('path' => $pathtodelete->path);

unlink($pathtodelete['path']);
/*
* Update MySQL
*/
$engine = ORM::forTable('mc_engines')->findOne($_POST['engine_id']);
$engine->delete();


Components\Page::redirect('../../engines.php?disp=engine_deleted&error=na');
?>
