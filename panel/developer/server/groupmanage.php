<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
    Components\Page::redirect('../../index.php?login');

$gamevers = ORM::forTable('mc_gameversion')->select('name')->findMany();

$gametypes = ORM::forTable('mc_gametypes')->select('mc_gametypes.id')->select('mc_gametypes.name')->select('mc_networks.name', 'network_name')->select('mc_networks.id', 'network_id')
    ->join('mc_networks', array('mc_gametypes.network_id', '=', 'mc_networks.id'))
    ->orderByDesc('network_id')
    ->findMany();

echo $twig->render(
    'developer/server/groupmanage.html', array(
        'gametype' => $gametypes,
        'footer' => array(
            'seconds' => number_format((microtime(true) - $pageStartTime), 4)
        )
    ));
?>