<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

$gamevers = ORM::forTable('mc_gameversion')->select('name')->findMany();
$enginee = ORM::forTable('mc_engines')->select('mc_engines.*')
			->orderByDesc('id')
			->findArray();

echo $twig->render(
	'developer/server/engines.html', array(
		'vername' => $gamevers,
		'engine' => $enginee,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>