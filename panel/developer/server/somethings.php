<?php

namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

$gamevers = ORM::forTable('mc_gameversion')->select('name')->findMany();
$gamevers = array_reverse($gamevers);

$networknames = ORM::forTable('mc_networks')->selectMany('id', 'name')->findMany();
$networknames = array_reverse($networknames);

echo $twig->render(
	'developer/server/somethings.html', array(
		'vername' => $gamevers,
		'networkname' => $networknames,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>