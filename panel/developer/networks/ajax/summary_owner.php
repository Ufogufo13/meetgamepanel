<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../index.php');
}

if(!isset($_POST['owner_id']))
    echo "Post Error. Refresh"; 

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

$user = ORM::forTable('users')
            ->select('users.email')->select('users.uuid')->select('register_time')
            ->where('users.id', $_POST['owner_id'])
            ->findOne();

if($user==NULL){
    echo '<div class="alert alert-info">Search ERROR. <a href="'.$core->settings->get('master_url').'mail.php?error_code=1'.'">Report this problem</a></div>';
}else{
    echo '<label for="user_info" class="control-label"><h5>User Info:</h5></label>
    <div name="user_info">
        e-mail: '.$user['email'].'</br></br>
        uuid: '.$user['uuid'].'</br></br>
        registered: '.gmdate("D, d/M/Y H:i:s", $user['register_time']).'
    </div>';
}

?>