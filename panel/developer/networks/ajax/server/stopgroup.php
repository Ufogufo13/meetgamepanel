<?php

namespace PufferPanel\Core;
use \ORM, \Unirest, \Tracy;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true) {

	if($core->user->getData('developer') == 0) {
		exit('You do not have the required permissions to perform this function.');
	}

	$forstart = ORM::forTable('servers')->where('gametype_id', $_POST['gametype_name'])->findMany();

	$totalservers = count($forstart);
	$rewrite = false;
	$errorMessage = "Unable to process your request. Please try again.";

	foreach ($forstart as &$forstart) {

		$nodedataa = ORM::forTable('nodes')->find_one($forstart['node']);

		if($_POST['killit'] != 1){
		    /*
			 * Connect and stop Function
			 */
			$get = Unirest::get(
				"http://".$nodedataa['ip'].":".$nodedataa['gsd_listen']."/gameservers/".$forstart['gsd_id']."/off",
				array(
					"X-Access-Token" => $forstart['gsd_secret']
				)
			);

			if($get->body != "ok") {
				exit($errorMessage." Unable to stop ".$forstart['name']." server (".$get->body->message.")");
			}

			echo $forstart['name']." stopped </br>";
		}else{
		    /*
			 * Connect and kill Function
			 */
			$get = Unirest::get(
				"http://".$nodedataa['ip'].":".$nodedataa['gsd_listen']."/gameservers/".$forstart['gsd_id']."/kill",
				array(
					"X-Access-Token" => $forstart['gsd_secret']
				)
			);

			if($get->body != "ok") {
				exit($errorMessage." Unable to kill ".$forstart['name']." server (".$get->body->message.")");
			}

			echo $forstart['name']." killed </br>";

		}
	}

} else {

	die('Invalid Authentication.');

}
Components\Page::redirect('../../groupmanage.php?disp=s_stopped');
?>

