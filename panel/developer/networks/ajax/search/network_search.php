<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	exit('<div class="alert alert-warning">Failed to Authenticate Account.</div>');
}

/*
 * Check Variables
 */
if(!isset($_POST['method'], $_POST['field'], $_POST['operator'], $_POST['term']))
	exit('<div class="alert alert-warning">Missing required variable.</div>');

if($_POST['method'] != 'simple')
	exit('<div class="alert alert-warning">Invalid Search Method.</div>');

if(empty($_POST['field']) || empty($_POST['operator']))
	exit('<div class="alert alert-warning">Required Variable Empty.</div>');

if(!in_array($_POST['field'], array('name', 'username', 'email', 'max_cpu', 'max_ram', 'max_disk', 'network_user')))
	exit('<div class="alert alert-warning">Required `field` contains unknown value.</div>');

if(!in_array($_POST['operator'], array('equal', 'not_equal', 'starts_w', 'ends_w', 'like')))
	exit('<div class="alert alert-warning">Required `operator` contains unknown value.</div>');

if(strlen($_POST['term']) < 4 && $_POST['field'] != 'max_cpu' && $_POST['field'] != 'max_ram' && $_POST['field'] != 'max_disk')
	exit('<div class="alert alert-warning">Required `term` must be at least 4 characters.</div>');

/*
 * Is Search Looking for Similar
 */
if($_POST['operator'] == 'starts_w'){
	$searchTerm = $_POST['term'].'%';
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'ends_w'){
	$searchTerm = '%'.$_POST['term'];
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'like'){
	$searchTerm = '%'.$_POST['term'].'%';
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'not_equal'){
	$searchTerm = $_POST['term'];
	$useOperator = '!=';
}else if($_POST['operator'] == 'equal'){
	$searchTerm = $_POST['term'];
	$useOperator = '=';
}

if($_POST['field'] == 'username' || $_POST['field'] == 'email'){

	$find = ORM::forTable('users')->rawQuery("SELECT * FROM `users` WHERE `".$_POST['field']."` ".$useOperator." :term", array('term' => $searchTerm))->findArray();
	$returnRows = '';
	foreach($find as &$row){
	$network = ORM::forTable('mc_networks')->select('mc_networks.*')->where('id', $row['network_id'])->findOne();
		$returnRows .= '
		<tr>
			<td><a href="view.php?id='.$network['id'].'">'.$network['name'].'</a></td>
			<td><a href="view.php?id='.$row['id'].'">'.$row['username'].'</a></td>
			<td><a href="view.php?id='.$row['id'].'">'.$row['email'].'</a></td>
			<td>'.$network['max_cpu'].'%</td>
			<td>'.$network['max_ram'].'MB</td>
			<td>'.$network['max_disk'].'MB</td>
			<td>'.date('r', $network['add_date']).'</td>
		</tr>
		';
	}

}else{

	$find = ORM::forTable('mc_networks')->rawQuery("SELECT * FROM `mc_networks` WHERE `".$_POST['field']."` ".$useOperator." :term", array('term' => $searchTerm))->findArray();
	$returnRows = '';
	foreach($find as &$row){
	$user = ORM::forTable('users')->select('id')->select('username')->select('email')->where('network_id', $row['id'])->findOne();
		$returnRows .= '
		<tr>
			<td><a href="view.php?id='.$row['id'].'">'.$row['name'].'</a></td>
			<td><a href="view.php?id='.$user['id'].'">'.$user['username'].'</a></td>
			<td><a href="view.php?id='.$user['id'].'">'.$user['email'].'</a></td>
			<td>'.$row['max_cpu'].'%</td>
			<td>'.$row['max_ram'].'MB</td>
			<td>'.$row['max_disk'].'MB</td>
			<td>'.date('r', $row['add_date']).'</td>
		</tr>
		';
	}
}
echo '<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Owner</th>
			<th>Owner Email</th>
			<th>CPU</th>
			<th>RAM</th>
			<th>DISK</th>
			<th>Created</th>
		</tr>
	</thead>
	<tbody>
		'.$returnRows.'
	</tbody>
</table>';

?>
