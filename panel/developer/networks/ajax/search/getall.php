<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	exit('<div class="error-box round">Failed to Authenticate Account.</div>');
}

$networks = ORM::forTable('mc_networks')->select('mc_networks.id', 'network_id')->select('mc_networks.name', 'network_name')->select('mc_networks.subusers', 'subowners')->select('users.username', 'username')->select('users.id', 'user_id')
			->left_outer_join('users', array('mc_networks.id', '=', 'users.network_id'))
			->findArray();

	$returnRows = '';
	$kej=0;
	foreach($networks as &$row){
		$gametypes = ORM::forTable('mc_gametypes')->select('id')->where('network_id', $row['network_id'])->findArray();
    	$servers = ORM::forTable('servers')->select('id')->where('network_id', $row['network_id'])->findArray();
		$returnRows .= '
		<tr>
			<td><a href="servers.php?goto='.$row['network_id'].'>'.$row['network_name'].'</a></td>
			<td><a href="servers.php?goto='.$row['user_id'].'>'.$row['username'].'</a></td>
			<td>'.$row['subowners'].'</td>
			<td>'.count($gametypes).'/'.count($servers).'</td>
			<td></td>
		</tr>
		';
		$kej++;
	}

echo '<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Owner</th>
			<th>Subowners</th>
			<th>gametypes/servers</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		'.$returnRows.'
	</tbody>
</table>';

?>
