<?php
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
    Components\Page::redirect('../../../index.php');
}

//Cookies :3
setcookie("__TMP_pp_admin_addengine", json_encode($_POST), time() + 30, '/', $core->settings->get('cookie_website'));

//jezeli klik add engine2
if(isset($_POST['engineadd_button']) && isset($_FILES['enginefile'])) {
    $errors1= array();
    $target_file = $engine_target_dir . basename($_FILES["enginefile"]["name"]);
    $file_name = $_FILES['enginefile']['name'];
    $file_size =$_FILES['enginefile']['size'];
    $file_tmp =$_FILES['enginefile']['tmp_name'];
    $file_type=$_FILES['enginefile']['type'];   
    $file_ext=strtolower(end(explode('.',$_FILES['enginefile']['name'])));
    $extensions = array("jar");
    $engine_controlsum = md5_file($_FILES["enginefile"]["tmp_name"]);
    if(in_array($file_ext,$extensions )=== false) {
        $errors1[]='Zle rozszerzenie, jar tylko';
    }
    if($file_size > 230000000) { //<25MB
        $errors1[]='Rozmiar pliku musi byc mniejszy niz 25MB';
    }  
    $fileCount = count(glob(''.$engine_target_dir.''.$_POST['engine_type']. "/" .$_POST['engine_type']. "-" .$_POST['engine_gamever']. '_*.jar'));
    $new_engine_Name = $engine_target_dir . $_POST['engine_type']. "/" . $_POST['engine_type'] ."-". $_POST['engine_gamever']. "_" . ( $fileCount + 1) .'.jar';
    $engine_buildek = $_POST['engine_type'] ."-". $_POST['engine_gamever']. "_" . ( $fileCount + 1) .'.jar';
    $controlsumcompare = ORM::forTable('mc_engines')->select('controlsum')->where('controlsum', $engine_controlsum)->findOne();
    if (!empty($controlsumcompare)){
        $errors1[]='taki juz istnieje';
        Components\Page::redirect('../../engines.php?disp=exist&error=na');
    }                   
    if(empty($errors1)==true) {
        move_uploaded_file($file_tmp,$new_engine_Name);  
        /*
        * Update MySQL
        */
        $engine = ORM::forTable('mc_engines')->create();
        $engine->set(array(
            'path' => $new_engine_Name,
            'file' => $engine_buildek,
            'gamever' => $_POST['engine_gamever'],
            'type' => $_POST['engine_type'],
            'controlsum' => $engine_controlsum,
            'info' => $_POST['engine_info'],
            'add_date' => time()
        ));
        $engine->save();
        echo '<h4>Dodano wersje silnika.</h4>';
    }else{
        Components\Page::redirect('../../engines.php?disp=size_or_extension&error=na');
    }   
} else {
    Components\Page::redirect('../../engines.php?disp=missing_args&error=na');
}

Components\Page::redirect('../../engines.php');
?>
