<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

if(!isset($_POST['node']))
	exit('No Node was Defined');

$node = ORM::forTable('nodes')->findOne($_POST['node']);
?>

<div class="form-group col-6 nopad">
	<label for="server_ip" class="control-label">Server IP</label>
	<div>
		<select name="server_ip" id="server_ip" class="form-control">
        <?php

			$ips = json_decode($node->ips, true);
			$i = 0;
			$hasFree = false;

			if(is_array($ips)){

				foreach($ips as $ip => $internal){

					if($i == 0)
						$firstIP = $ip;

		            if($internal['ports_free'] > 0){
		            	$hasFree = true;
						$append = ($internal['ports_free'] > 1) ? "s" : null;
						echo '<option value="'.$ip.'">'.$ip.' ('.$internal['ports_free'].' Avaliable Port'.$append.')</option>';
		            }else
					  echo '<option disabled="disabled">'.$ip.' (no ports avaliable)</option>';
	                $i++;

				}

			}

		?>
    	</select>
	</div>
</div>
<div class="form-group col-6 nopad-right">
	<label for="server_ip" class="control-label">Server Port</label>
		<div>
  		<?php

	        $ports = json_decode($node->ports, true);

	        if(!empty($ports)){

                foreach($ports as $ip => $internal){

    	            if($firstIP == $ip)
    	                echo '<span id="node_'.$ip.'"><select name="server_port_'.$ip.'" class="form-control">';
    	            else
    	                echo '<span style="display:none;" id="node_'.$ip.'"><select name="server_port_'.$ip.'" class="form-control">';

    	                foreach($internal as $port => $avaliable){

                            if($avaliable == 1)
                                echo '<option value="'.$port.'">'.$port.'</option>';
                            else
                                echo '<option disabled="disabled">'.$port.' (in use)</option>';

    	                }

    	            echo '</select></span>';

    	        }

            }

  		?>
  		</div>
</div>
<?php

	if($hasFree === false)
		echo "<script type=\"text/javascript\">$('#noPorts').show();</script>";
?>
