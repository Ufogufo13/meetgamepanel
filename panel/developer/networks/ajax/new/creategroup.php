<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true) {
	Components\Page::redirect('../../../index.php');
}

/*
 * path build
 */
$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();
    
$networkk = ORM::forTable('mc_networks')->select('name')->where('id', $networkId->network_id)->findOne();
$networkk = array('name' => $networkk->name);

$gametypee = ORM::forTable('mc_gametypes')->select('name')->where('id', $_POST['gametype_name'])->findOne();
$gametypee = array('name' => $gametypee->name);

$enginee = ORM::forTable('mc_engines')->selectMany('path', 'file', 'controlsum')->where('id', $_POST['engine_name'])->findOne();
$enginee = array('path' => $enginee->path,
				 'controlsum' => $enginee->controlsum,
				 'name' => $enginee->file);

setcookie("__TMP_pp_admin_newgroupserver", json_encode($_POST), time() + 30, '/');

/*
* Set Values
*/
@$_POST['server_port'] = $_POST['server_port_'.str_replace('.', '_', $_POST['server_ip'])];

/*
 * Are they all Posted?
 */
if(!isset($_POST['server_name'], $_POST['node'], $_POST['email'], $_POST['server_ip'], $_POST['server_port'], $_POST['alloc_mem'], $_POST['alloc_disk'], $_POST['ftp_pass'], $_POST['ftp_pass_2'], $_POST['cpu_limit'])) {
	Components\Page::redirect('../../add.php?disp=missing_args&error=na');
}

/*
* Determine if Node (IP & Port) is Avaliable
*/
$node = ORM::forTable('nodes')->findOne($_POST['node']);

if(!$node) {
	Components\Page::redirect('../../add.php?error=node&disp=n_fail');
}

/*
* GSD Must Be Online!
*/
if(!@fsockopen($node->ip, 8003, $num, $error, 3)) {
	Components\Page::redirect('../../add.php?disp=gsd_offline&error=na');
}

/*
* Validate IP & Port
*/
$ips = json_decode($node->ips, true);
$ports = json_decode($node->ports, true);

if(!array_key_exists($_POST['server_ip'], $ips)) {
	Components\Page::redirect('../../add.php?error=server_ip&disp=ip_fail');
}
if(!array_key_exists($_POST['server_port'], $ports[$_POST['server_ip']])) {
	Components\Page::redirect('../../add.php?error=server_port&disp=port_fail');
}
if($ports[$_POST['server_ip']][$_POST['server_port']] == 0) {
	Components\Page::redirect('../../add.php?error=server_port&disp=port_full');
}

/*
 * Validate Email
 */
if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
	Components\Page::redirect('../../add.php?error=email&disp=e_fail');
}

$user = ORM::forTable('users')->select('id')->where('email', $_POST['email'])->findOne();
if(!$user) {
	Components\Page::redirect('../../add.php?error=email&disp=a_fail');
}

/*
 * Validate Disk & Memory
 */
if(!is_numeric($_POST['alloc_mem']) || !is_numeric($_POST['alloc_disk'])) {
	Components\Page::redirect('../../add.php?error=alloc_mem|alloc_disk&disp=m_fail');
}

/*
 * Validate start and end number of server
 */
if(!is_numeric($_POST['server_start_number']) || !is_numeric($_POST['server_end_number'])) {
	Components\Page::redirect('../../add.php?error=server_start_number|server_end_number&disp=nmb_fail');
}

/*
 * Validate CPU Limit
 */
if(!is_numeric($_POST['cpu_limit'])) {
	Components\Page::redirect('../../add.php?error=cpu_limit&disp=cpu_limit');
}


/*
 * Validate ftp Password
 */
if($_POST['ftp_pass'] != $_POST['ftp_pass_2'] || strlen($_POST['ftp_pass']) < 8) {
	Components\Page::redirect('../../add.php?error=ftp_pass|ftp_pass_2&disp=p_fail');
}

$servernamee = ORM::forTable('servers')->select('name')
		->where(array('name' => $gametypee['name'].$_POST['server_start_number'],
			            'network_id' => $networkId->network_id
			        ))
		->findOne();

$numbeeer = $_POST['server_end_number'] - $_POST['server_start_number'];

for(; $numbeeer > -1; $numbeeer--){
	if($servernamee) {
		echo $gametypee['name'].$_POST['server_start_number']." not created (EXIST) </br>";
		$_POST['server_start_number']++;
	}else{
		$iv = $core->auth->generate_iv();
		$_POST['ftp_pass'] = $core->auth->encrypt($_POST['ftp_pass'], $iv);
		$ftpUser = Components\Functions::generateFTPUsername($gametypee['name']);
		$ftpUsergroup = Components\Functions::generateFTPUsernamegroup($gametypee['name']);
		$ftpUsernetwork = Components\Functions::generateFTPUsernamenetwork($networkk['name']);

		$serverHash = $core->auth->generateUniqueUUID('servers', 'hash');
		$gsdSecret = $core->auth->generateUniqueUUID('servers', 'gsd_secret');

		$data = array(
			"name" => $serverHash,
			"srvname" => $gametypee['name'].$_POST['server_start_number'],
			"user" => $ftpUser,
		    "gametype" => $gametypee['name'],
		    "network" => $ftpUsernetwork,
		    "group" => $ftpUsergroup,
		    "main_folder" => $node->gsd_server_dir.$ftpUsernetwork,
			"overide_command_line" => "",
		    "path" => $node->gsd_server_dir.$ftpUsernetwork."/".$gametypee['name']."/".$gametypee['name'].$_POST['server_start_number'],
			"build" => array(
				"install_dir" => $node->gsd_server_dir.$ftpUsernetwork."/init",
				"disk" => array(
					"hard" => ($_POST['alloc_disk'] < 32) ? 32 : (int) $_POST['alloc_disk'],
					"soft" => ($_POST['alloc_disk'] > 2048) ? (int) $_POST['alloc_disk'] - 1024 : 32
				),
				"cpu" => (int) $_POST['cpu_limit'],
		        "controlsum" => $enginee['controlsum'],
		        "prescreen" => $networkk['name'],
		        "engine_name" => $enginee['name'],
				"engine_path" => $enginee['path']
			),
			"variables" => array(
		    	"-jar" => $networkk['name']."_".$gametypee['name'].$_POST['server_start_number'].".jar",
				"-Xmx" => $_POST['alloc_mem']."M",
			    "-Xms" => $_POST['alloc_mem_init']."M",
			    "-XX:ParallelGCThreads=" => $_POST['threads']
			),
			"keys" => array(
				$gsdSecret => array("s:ftp", "s:get", "s:power", "s:files", "s:files:get", "s:files:put", "s:files:zip", "s:query", "s:console", "s:console:send")
			),
			"gameport" => (int) $_POST['server_port'],
			"gamehost" => $_POST['server_ip'],
			"plugin" => "minecraft",
			"autoon" => false
		);

		$data = json_encode($data);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://'.$node->ip.':'.$node->gsd_listen.'/gameservers');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'X-Access-Token: '.$node['gsd_secret']
		));
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "settings=".$data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$content = json_decode(curl_exec($ch), true);
		curl_close($ch);

		$server = ORM::forTable('servers')->create();
		$server->set(array(
			'gsd_id' => $content['id'],
			'hash' => $serverHash,
			'gsd_secret' => $node->gsd_secret,
			'encryption_iv' => $iv,
			'node' => $_POST['node'],
			'name' => $gametypee['name'].$_POST['server_start_number'],
			'modpack' => '0000-0000-0000-0',
			'server_jar' => $networkk['name']."_".$gametypee['name'].$_POST['server_start_number'].".jar",
			'owner_id' => $user->id,
			'max_ram' => $_POST['alloc_mem'],
			'init_ram' => $_POST['alloc_mem_init'],
			'disk_space' => $_POST['alloc_disk'],
			'cpu_limit' => $_POST['cpu_limit'],
			'threads_limit' => $_POST['threads'],
			'date_added' => time(),
			'server_ip' => $_POST['server_ip'],
			'server_port' => $_POST['server_port'],
			'ftp_user' => $ftpUser,
			'ftp_group' => $ftpUsergroup,
			'ftp_pass' => $_POST['ftp_pass'],
			'network_id' => $networkId->network_id,
			'gametype_id' => $_POST['gametype_name'],
			'engine_id' => $_POST['engine_name']
		));
		$server->save();

		$ips[$_POST['server_ip']]['ports_free']--;
		$ports[$_POST['server_ip']][$_POST['server_port']]--;

		$node->ips = json_encode($ips);
		$node->ports = json_encode($ports);
		$node->save();

		$core->email->buildEmail('admin_new_server', array(
				'NAME' => $_POST['server_name'],
				'FTP' => $node->fqdn.':21',
				'MINECRAFT' => $node->fqdn.':'.$_POST['server_port'],
				'USER' => $ftpUser,
				'PASS' => $_POST['ftp_pass_2']
		))->dispatch($_POST['email'], $core->settings->get('company_name').' - New Server Added');
		echo $gametypee['name'].$_POST['server_start_number']." created </br>";
		sleep(1);
		$_POST['server_start_number']++;
		$_POST['server_port']++;
	}
	$servernamee = ORM::forTable('servers')->select('name')
			->where(array('name' => $gametypee['name'].$_POST['server_start_number'],
			            'network_id' => $networkId->network_id
			        ))
			->findOne();
}

?>
