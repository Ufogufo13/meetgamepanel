<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../../index.php?login');

if(isset($_GET['success']) && $_GET['success']) {
	$outputMessage = '<div class="alert alert-success">URL settings saved.</div>';
}

echo $twig->render(
	'developer/config/urls.html', array(
		'output' => (isset($outputMessage)) ? $outputMessage : null,
		'settings' => $core->settings->get(),
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>