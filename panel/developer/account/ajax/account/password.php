<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../../../index.php?login');
}

if(!isset($_POST['uid']) || !is_numeric($_POST['uid']))
	Components\Page::redirect('../../find.php?error=UPDATE-USER__undefined_user');

if($_POST['pass'] != $_POST['pass_2'])
	Components\Page::redirect('../../view.php?id='.$_POST['uid'].'&error=password');

$user = ORM::forTable('users')->findOne($_POST['uid']);
$user->password = $core->auth->hash($_POST['pass']);

	if(isset($_POST['email_user'])){

		/*
		 * Send Email
		 */
		$core->email->buildEmail('new_password', array(
            'NEW_PASS' => $_POST['pass'],
            'EMAIL' => $_POST['email']
        ))->dispatch($_POST['email'], $core->settings->get('company_name').' - An Admin has Reset Your Password');

	}

	if(isset($_POST['clear_session'])){

		$user->session_id = null;
		$user->session_ip = null;

	}

$user->save();

Components\Page::redirect('../../view.php?id='.$_POST['uid'].'&disp=p_updated');

?>
