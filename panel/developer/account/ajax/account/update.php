<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../../../index.php?login');
}

if(!isset($_POST['uid']) || !is_numeric($_POST['uid']))
	Components\Page::redirect('../find.php?error=UPDATE-USER__undefined_user');

if($_POST['action'] == 'details'){

	$user = ORM::forTable('users')->findOne($_POST['uid']);
	$user->set(array(
		'email' => $_POST['email'],
		'root_admin' => $_POST['root_admin']
	));
	$user->save();

	Components\Page::redirect('../../view.php?id='.$_POST['uid'].'&disp=d_updated');

}

?>
