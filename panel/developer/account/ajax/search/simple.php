<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	exit('<div class="alert alert-warning">Failed to Authenticate Account.</div>');
}

/*
 * Check Variables
 */
if(!isset($_POST['method'], $_POST['field'], $_POST['operator'], $_POST['term']))
	exit('<div class="alert alert-warning">Missing required variable.</div>');

if($_POST['method'] != 'simple')
	exit('<div class="alert alert-warning">Invalid Search Method.</div>');

if(empty($_POST['field']) || empty($_POST['operator']))
	exit('<div class="alert alert-warning">Required Variable Empty.</div>');

if(!in_array($_POST['field'], array('email', 'username', 'root_admin', 'developer')))
	exit('<div class="alert alert-warning">Required `field` contains unknown value.</div>');

if(!in_array($_POST['operator'], array('equal', 'not_equal', 'starts_w', 'ends_w', 'like')))
	exit('<div class="alert alert-warning">Required `operator` contains unknown value.</div>');

if(strlen($_POST['term']) < 4 && $_POST['field'] != 'root_admin' && $_POST['field'] != 'developer')
	exit('<div class="alert alert-warning">Required `term` must be at least 4 characters.</div>');

if($_POST['field'] == 'root_admin' && !in_array($_POST['term'], array('0', '1')))
	exit('<div class="alert alert-warning">Required `term` for root_admin must be 1 or 0.</div>');

if($_POST['field'] == 'developer' && !in_array($_POST['term'], array('0', '1')))
	exit('<div class="alert alert-warning">Required `term` for developer must be 1 or 0.</div>');
/*
 * Is Search Looking for Similar
 */
if($_POST['operator'] == 'starts_w'){
	$searchTerm = $_POST['term'].'%';
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'ends_w'){
	$searchTerm = '%'.$_POST['term'];
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'like'){
	$searchTerm = '%'.$_POST['term'].'%';
	$useOperator = 'LIKE';
}else if($_POST['operator'] == 'not_equal'){
	$searchTerm = $_POST['term'];
	$useOperator = '!=';
}else if($_POST['operator'] == 'equal'){
	$searchTerm = $_POST['term'];
	$useOperator = '=';
}


$find = ORM::forTable('users')->rawQuery("SELECT * FROM `users` WHERE `".$_POST['field']."` ".$useOperator." :term", array('term' => $searchTerm))->findMany();

	$returnRows = '';
	foreach($find as &$row){

		$isRoot = ($row['root_admin'] == 1) ? '<span class="label label-danger">Admin</span>' : '<span class="label label-success">User</span>';
		$isdeveloper = ($row['developer'] == 1) ? '<span class="label label-danger">dev</span>' : '';

		$returnRows .= '
		<tr>
			<td><a href="view.php?id='.$row['id'].'">'.$row['username'].'</a></td>
			<td>'.$row['email'].'</td>
			<td>'.gmdate("D, d/M/Y H:i:s", $row['register_time']).'</td>
			<td style="text-align:center;">'.$isRoot.' '.$isdeveloper.'</td>
		</tr>
		';

	}

echo '<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>Username</th>
			<th>Email</th>
			<th>Registered</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		'.$returnRows.'
	</tbody>
</table>';

?>
