<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true) {

	Components\Page::redirect('../../../index.php');

}

if(!isset($_POST['add_ports_node'])) {

	Components\Page::redirect('../../list.php');

}

if(!isset($_POST['add_ports'], $_POST['add_ports_ip'])) {

	Components\Page::redirect('../../view.php?id='.$_POST['add_ports_node'].'&tab=allocation');

}

if(!preg_match('/^[\d-,]+$/', $_POST['add_ports'])) {

	Components\Page::redirect('../../view.php?id='.$_POST['add_ports_node'].'&disp=add_port_fail&tab=allocation');

}

$portList = Components\Functions::processPorts($_POST['add_ports']);
$node = ORM::forTable('nodes')->findOne($_POST['add_ports_node']);
$saveips = json_decode($node->ips, true);
$saveports = json_decode($node->ports, true);

foreach($portList as $id => $port) {

	if(strlen($port) < 6 && strlen($port) > 0 && array_key_exists($_POST['add_ports_ip'], $saveports) && !array_key_exists($port, $saveports[$_POST['add_ports_ip']])) {

		$saveports[$_POST['add_ports_ip']][$port] = 1;
		$saveips[$_POST['add_ports_ip']]['ports_free']++;

	}

}

$node->ips = json_encode($saveips);
$node->ports = json_encode($saveports);
$node->save();

Components\Page::redirect('../../view.php?id='.$_POST['add_ports_node'].'&tab=allocation');
?>
