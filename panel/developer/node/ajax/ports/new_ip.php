<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */

namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true) {

	Components\Page::redirect('../../../index.php');

}

if(!isset($_POST['ip_port'])) {

	Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&disp=missing_args');

}

/*
 * Clean Inputs
 */
$_POST['ip_port'] = str_replace(" ", "", $_POST['ip_port']);

$lines = explode("\r\n", $_POST['ip_port']);

/*
 * Unpack Variables
 */
$node = ORM::forTable('nodes')->findOne($_POST['nid']);

$IPA = json_decode($node->ips, true);
$IPP = json_decode($node->ports, true);

foreach($lines as $id => $values) {

	list($ip, $ports) = explode('|', $values);

	$IPA = array_merge($IPA, array($ip => array()));
	$IPP = array_merge($IPP, array($ip => array()));

	$portList = [];
	$portList = Components\Functions::processPorts($ports);

	for($l=0; $l<count($portList); $l++) {

			$IPP[$ip][$portList[$l]] = 1;

	}

	/*
	 * Make sure Ports are in the array
	 */
	if(count($IPP[$ip]) > 0) {

		$IPA[$ip] = array_merge($IPA[$ip], array("ports_free" => count($IPP[$ip])));

	} else {

		Components\Page::redirect('../../view.php?id='.$_POST['nid'].'&error=ip_port&disp=ip_port_space');

	}

}

$node->ips = json_encode($IPA);
$node->ports = json_encode($IPP);
$node->save();

Components\Page::redirect('../../view.php?id='.$_POST['nid']);
?>
