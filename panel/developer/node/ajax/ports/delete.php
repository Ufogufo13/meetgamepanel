<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

if(!isset($_POST['node'], $_POST['port'], $_POST['ip']))
	exit('POST Only');

/*
 * Verify port is Real & Not in Use
 */
$node = ORM::forTable('nodes')->findOne($_POST['node']);

if($node === false)
	exit('Invalid Node');

$ips = json_decode($node->ips, true);
$ports = json_decode($node->ports, true);

if(array_key_exists($_POST['ip'], $ports) && array_key_exists($_POST['port'], $ports[$_POST['ip']]) && $ports[$_POST['ip']][$_POST['port']] == 1){

	unset($ports[$_POST['ip']][$_POST['port']]);
	$ips[$_POST['ip']]['ports_free'] = ($ips[$_POST['ip']]['ports_free'] - 1);

}else
	exit('No Port/IP or Port in Use');

$node->ips = json_encode($ips);
$node->ports = json_encode($ports);
$node->save();

die('Done');

?>