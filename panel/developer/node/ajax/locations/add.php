<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../../index.php');
}

setcookie("__TMP_pp_admin_newlocation", json_encode($_POST), time() + 30, '/');

if(!isset($_POST['shortcode'], $_POST['location'])) {
	Components\Page::redirect('../../locations.php?error=missing_args');
}

if(!preg_match('/^[\w-]{1,10}$/', $_POST['shortcode'])) {
	Components\Page::redirect('../../locations.php?error=shortcode');
}

if(empty($_POST['location'])) {
	Components\Page::redirect('../../locations.php?error=location');
}

$location = ORM::forTable('locations')->create();
$location->short = $_POST['shortcode'];
$location->long = $_POST['location'];
$location->save();

Components\Page::redirect('../../locations.php?success');