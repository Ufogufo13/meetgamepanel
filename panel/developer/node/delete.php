<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;

use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../index.php?login');
}

$nodes = ORM::forTable('nodes')->selectMany('nodes.id', 'nodes.node')
		->left_outer_join('servers', array('nodes.id', '=', 'servers.node'))
		->where_null('servers.id')
		->findMany();

if(isset($_GET['node'])) {

	if($_GET['node'] === 'unknown') {
		$outputMessage = '<div class="alert alert-danger">Selected node does not exist</div>';
	} else if ($_GET['node'] === 'notempty') {
		$outputMessage = '<div class="alert alert-danger">Selected node has servers on it</div>';
	}

}

echo $twig->render(
	'developer/node/delete.html', array(
		'output' => isset($outputMessage) ? $outputMessage : null,
		'nodes' => $nodes,
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));