<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../index.php?login');
}

$nodes = ORM::forTable('nodes')
		->select('nodes.*')->select('locations.long', 'l_location')
		->join('locations', array('nodes.location', '=', 'locations.short'))
		->findMany();

echo $twig->render(
    'developer/node/list.html', array(
        'nodes' => $nodes,
        'footer' => array(
            'seconds' => number_format((microtime(true) - $pageStartTime), 4)
        )
    ));
?>