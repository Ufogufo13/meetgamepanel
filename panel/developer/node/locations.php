<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true){
	Components\Page::redirect('../../index.php?login');
}

$locations = ORM::forTable('locations')
			->select_many('locations.*')
			->select_expr('COUNT(nodes.id)', 'totalnodes')
			->left_outer_join('nodes', array('locations.short', '=', 'nodes.location'))
			->group_by('locations.id')
			->find_many();

echo $twig->render(
	'developer/node/locations.html', array(
	'locations' => $locations,
	'footer' => array(
		'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
	));
?>