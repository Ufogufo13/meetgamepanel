<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), null, true) !== true)
	Components\Page::redirect('../index.php?login');

$head = trim(file_get_contents('../../.git/HEAD'));
if(is_array(explode('/', $head))){
	list($ignore, $path) =  explode(" ", $head);
	$git = '('.$head.') (sha: '.substr(trim(file_get_contents('../../.git/'.$path)), 0 ,8).')';
}else
	$git = $head;

$networkId = ORM::forTable('users')->select('network_id')
    ->where('users.id', $core->user->getData('id'))
    ->findOne();

echo $twig->render(
        'developer/index.html', array(
            'version' => trim(file_get_contents('../../src/versions/current')).' '.$git,
            'installer' => (is_dir('../install')) ? true : false,
            'footer' => array(
                'seconds' => number_format((microtime(true) - $pageStartTime), 4)
            )
        ));
?>
