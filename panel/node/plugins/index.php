<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

$filesIncluded = true;

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false)
	Components\Page::redirect($core->settings->get('master_url').'index.php?login');

/*
 * Display Page
 */
echo $twig->render(
		'node/plugins/index.html', array(
            'server' => $core->server->getData(),
			'footer' => array(
				
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));
?>
