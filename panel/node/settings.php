<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false)
	Components\Page::redirect($core->settings->get('master_url').'index.php?login');

if($core->user->hasPermission('manage.view') !== true)
	Components\Page::redirect('index.php?error=no_permission');

if(isset($_GET['do']) && $_GET['do'] == 'generate_password')
	exit($core->auth->keygen(mt_rand(12, 18)));

$enginess = ORM::forTable('mc_engines')->selectMany('id', 'file')->findMany();
$enginess = array_reverse($enginess);
/*
 * Display Page
 */
echo $twig->render(
		'node/settings.html', array(
			'server' => array_merge($core->server->getData(), array('server_jar' => (str_replace(".jar", "", $core->server->getData('server_jar'))))),
			'node' => array(
				'fqdn' => $core->server->nodeData('fqdn')
			),
			'engine' => $enginess,
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));
?>
