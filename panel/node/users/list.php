<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false){

	Components\Page::redirect($core->settings->get('master_url').'index.php?login');
	exit();
}

if($core->user->hasPermission('users.view') !== true)
	Components\Page::redirect('../index.php?error=no_permission');

$access = json_decode($core->server->getData('subusers'), true);
$users = array();

if($core->settings->get('allow_subusers') == 1){

	if(is_array($access) && !empty($access)){

		foreach($access as $id => $status) {

			if($status != "verified"){

				$user = ORM::forTable('account_change')->select('content')->where(array('key' => $status, 'verified' => 0))->findOne();

				if($user) {

					$_perms = json_decode($user->content, true);
					$users = array_merge($users, array($id => array("status" => "pending", "revoke" => urlencode($status), "permissions" => $_perms[$core->server->getData('hash')]['perms'])));

				}

			}else{

				$user = ORM::forTable('users')->selectMany('permissions', 'email', 'uuid')->where('id', $id)->findOne();
				$_perms = json_decode($user->permissions, true);
				$users = array_merge($users, array($user->email => array("status" => "verified", "id" => $user->uuid, "permissions" => $_perms[$core->server->getData('hash')]['perms'])));

			}

		}

	}

}

/*
* Display Page
*/
echo $twig->render(
		'node/users/list.html', array(
			'users' => $users,
			'server' => $core->server->getData(),
			'allow_subusers' => $core->settings->get('allow_subusers'),
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));
?>