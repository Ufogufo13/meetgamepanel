<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \League\Flysystem\Filesystem as Filesystem, \League\Flysystem\Adapter\Ftp as Adapter;

require_once '../../../../src/core/core.php';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false) {
	exit('Not authenticated.');
}

if($core->user->hasPermission('files.delete') !== true) {
	exit('You do not have permission to delete files.');
}

if(!isset($_POST['deleteItemType'], $_POST['deleteItemPath'])) {
	exit('Not enough variables were passed.');
}

/*
 * Delete File
 */
if($_POST['deleteItemType'] == 'file' && !empty($_POST['deleteItemPath'])) {

	try {

		$cid = ftp_ssl_connect($core->server->nodeData('ip'));
		$login = @ftp_login($cid, $core->server->getData('ftp_user').'-'.$core->server->getData('gsd_id'), $core->auth->decrypt($core->server->getData('ftp_pass'), $core->server->getData('encryption_iv')));

		// What idiot designed this function like this?
		if(!$login) {
			throw new \Exception("Unable to login to the FTP server!");
		}

		$_POST['deleteItemPath'] = urldecode($_POST['deleteItemPath']);

		ftp_delete($cid, $_POST['deleteItemPath']);
		ftp_close($cid);
		echo 'ok';

	} catch(\Exception $e) {
		exit('Error occured trying to delete that file! '.$e->getMessage());
	}

} else if($_POST['deleteItemType'] == 'dir' && !empty($_POST['deleteItemPath'])) {

	try {

		$filesystem = new Filesystem(new Adapter(array(
			'host' => $core->server->nodeData('ip'),
			'username' => $core->server->getData('ftp_user').'-'.$core->server->getData('gsd_id'),
			'password' => $core->auth->decrypt($core->server->getData('ftp_pass'), $core->server->getData('encryption_iv')),
			'port' => 21,
			'passive' => true,
			'ssl' => true,
			'timeout' => 10
		)));

	} catch(\Exception $e) {
		\Tracy\Debugger::log($e);
		exit('An execption occured.');
	}

	try {

		$_POST['deleteItemPath'] = urldecode($_POST['deleteItemPath']);
		$filesystem->deleteDir($_POST['deleteItemPath']);
		echo 'ok';

	} catch(\League\Flysystem\FileNotFoundException $e){
		exit('Error trying to delete that directory. '.$e->getMessage());
	}

} else {
	var_dump($_POST);
	echo 'Nothing was matched in the script.';
}