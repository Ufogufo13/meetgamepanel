<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \Unirest;

require_once '../../../../src/core/core.php';

if(!$core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash'))) {
	exit('Not authenticated.');
}

if($core->user->hasPermission('files.zip') !== true) {

	exit('You do not have permission to zip or unzip files.');

}

if(isset($_POST['zipItemPath']) && !empty($_POST['zipItemPath'])) {

	$request = Unirest::put(
		"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/file/".$_POST['zipItemPath'],
		array(
			"X-Access-Token" => $core->server->getData('gsd_secret')
		),
		array(
			"zip" => $_POST['zipItemPath']
		)
	);

	if($request->code !== 200) {

		exit("An error was enocuntered trying to process your request. Server returned the following data\n\n[HTTP/1.1 {$request->code}] {$request->raw_body}");

	}

} elseif(isset($_POST['unzipItemPath']) && !empty($_POST['unzipItemPath'])) {

	$request = Unirest::put(
		"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/file/".$_POST['unzipItemPath'],
		array(
			"X-Access-Token" => $core->server->getData('gsd_secret')
		),
		array(
			"unzip" => $_POST['unzipItemPath']
		)
	);

	if($request->code !== 200) {

		exit("An error was enocuntered trying to process your request. Server returned the following data\n\n[HTTP/1.1 {$request->code}] {$request->raw_body}");

	}

} else {

	exit("Error trying to handle an unknown function.");

}