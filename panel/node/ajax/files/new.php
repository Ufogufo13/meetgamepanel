<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \Tracy, \League\Flysystem\Filesystem as Filesystem, \League\Flysystem\Adapter\Ftp as Adapter;

require_once '../../../../src/core/core.php';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false) {
	exit('<div class="alert alert-danger">Not authenticated.</div>');
}

if($core->user->hasPermission('files.delete') !== true) {
	exit('<div class="alert alert-danger">You do not have permission to delete files.</div>');
}

if(!isset($_POST['newFilePath'], $_POST['newFileContents'])) {
	exit('<div class="alert alert-danger">Missing some required parameters.</div>');
}

try {

	$filesystem = new Filesystem(new Adapter(array(
		'host' => $core->server->nodeData('ip'),
		'username' => $core->server->getData('ftp_user').'-'.$core->server->getData('gsd_id'),
		'password' => $core->auth->decrypt($core->server->getData('ftp_pass'), $core->server->getData('encryption_iv')),
		'port' => 21,
		'passive' => true,
		'ssl' => true,
		'timeout' => 10
	)));

} catch(\Exception $e) {

	http_response_code(500);
	Tracy\Debugger::log($e);
	exit('<div class="alert alert-danger">An execption occured when trying to connect to the server.</div>');

}

try {

	if(!$filesystem->write(urldecode($_POST['newFilePath']), $_POST['newFileContents'])) {

		http_response_code(500);
		exit('<div class="alert alert-danger">An error occured when trying to write this file to the system.</div>');

	} else {
		exit('ok');
	}

} catch(\Exception $e) {

	http_response_code(500);
	Tracy\Debugger::log($e);
	exit('<div class="alert alert-danger">An execption occured when trying to write the file to the server.</div>');

}