<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \Unirest, \Tracy;

require_once '../../../../src/core/core.php';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true) {

	if(!$core->user->hasPermission('console.power')) {
		exit('You do not have the required permissions to perform this function.');
	}

	/*
	 * Open Stream for Reading/Writing
	 */
	$rewrite = false;
	$errorMessage = "Unable to process your request. Please try again.";

	try {

		$response = Unirest::get(
			"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/file/server.properties",
			array(
				"X-Access-Token" => $core->server->getData('gsd_secret')
			)
		);

	} catch(\Exception $e) {

		Tracy\Debugger::log($e);
		exit($errorMessage." Unable to connect to remote host.");

	}

	/*
	 * Typically Means Server is Off
	 */
	if(!in_array($response->code, array(200, 500))) {
		switch($response->code) {

			case 403:
				exit($errorMessage." Authentication error encountered.");
				break;
			default:
				exit("$errorMessage HTTP/$response->code. Invalid response was recieved. ($response->raw_body)");
				break;

		}
	}

	if($response->code == 500 || !isset($response->body->contents) || empty($response->body->contents)) {

		/*
		 * Create server.properties
		 */
		if(!file_exists(APP_DIR.'templates/server.properties.tpl') || empty(file_get_contents(APP_DIR.'templates/server.properties.tpl')))
			exit($errorMessage." No Template Avaliable for server.properties");

		$put = Unirest::put(
			"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/file/server.properties",
			array(
				"X-Access-Token" => $core->server->getData('gsd_secret')
			),
			array(
				"contents" => sprintf(file_get_contents(APP_DIR.'templates/server.properties.tpl'), $core->server->getData('server_port'), $core->server->getData('server_ip'))
			)
		);

        if(!empty($put->body)) {
        	exit($errorMessage." Unable to make server.properties");
		}

		$core->log->getUrl()->addLog(0, 1, array('system.create_serverprops', 'A new server.properties file was created for your server.'));

	}

    /*
	 * Connect and Run Function
	 */
	$get = Unirest::get(
		"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/on",
		array(
			"X-Access-Token" => $core->server->getData('gsd_secret')
		)
	);

	if($get->body != "ok") {
		exit($errorMessage." Unable to start server (".$get->body->message.")");
	}

	echo 'ok';

} else {

	die('Invalid Authentication.');

}
?>
