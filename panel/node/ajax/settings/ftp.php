<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;

require_once('../../../../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true){

	if($core->user->hasPermission('manage.ftp.password') !== true)
		Components\Page::redirect('../../index.php?error=no_permission');

	if(!isset($_POST['ftp_pass'], $_POST['ftp_pass_2']))
		Components\Page::redirect('../../settings.php?error=ftp_pass|ftp_pass_2&disp=no_pass');

	if(strlen($_POST['ftp_pass']) < 8)
		Components\Page::redirect('../../settings.php?error=ftp_pass|ftp_pass_2&disp=pass_len');

	if($_POST['ftp_pass'] != $_POST['ftp_pass_2'])
		Components\Page::redirect('../../settings.php?error=ftp_pass|ftp_pass_2&disp=pass_match');

	/*
	 * Update Server ftp Information
	 */
	$iv = $core->auth->generate_iv();

	$ftp = ORM::forTable('servers')->findOne($core->server->getData('id'));
	$ftp->ftp_pass = $core->auth->encrypt($_POST['ftp_pass'], $iv);
	$ftp->encryption_iv = $iv;
	$ftp->save();

	Components\Page::redirect('../../settings.php?success');

}

?>
