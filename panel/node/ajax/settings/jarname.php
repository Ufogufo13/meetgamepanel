<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM, \Unirest;

require_once('../../../../src/core/core.php');

if ($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === true) {

	if ($core->user->hasPermission('manage.rename.jar') !== true) {
		Components\Page::redirect('../../index.php?error=no_permission');
	}

	if (!isset($_POST['jarfile']) || empty($_POST['jarfile'])) {
		Components\Page::redirect('../../settings.php');
	}

	if (!preg_match('/^([\w\d_.-]+)$/', $_POST['jarfile'])) {
		Components\Page::redirect('../../settings.php');
	}

	$enginee = ORM::forTable('mc_engines')->selectMany('path', 'file', 'controlsum')->where('id', $_POST['jarfile'])->findOne();
	$enginee = array('path' => $enginee->path,
					 'controlsum' => $enginee->controlsum,
					 'name' => $enginee->file);

	$networkId = ORM::forTable('users')->select('network_id')
	    ->where('users.id', $core->user->getData('id'))
	    ->findOne();

	$networkk = ORM::forTable('mc_networks')->select('name')->where('id', $networkId->network_id)->findOne();

	/*
	 * Update It
	 */
	$server = ORM::forTable('servers')->findOne($core->server->getData('id'));
	$server->server_jar = $networkk->name."_".$server->name.".jar";
	$server->engine_id = $_POST['jarfile'];
	$server->save();

	/*
	 * Update GSD Setting
	 */
	$request = Unirest::put(
		'http://' . $core->server->nodeData('ip') . ':' . $core->server->nodeData('gsd_listen') . '/gameservers/' . $core->server->getData('gsd_id'),
		array(
			"X-Access-Token" => $core->server->nodeData('gsd_secret')
		),
		array(
			"variables" => json_encode(array(
				"-jar" => $networkk->name."_".$server->name.".jar",
				"-Xmx" => $server->max_ram."M",
				"-Xms" => $server->init_ram."M",
				"-XX:ParallelGCThreads=" => $server->threads_limit
			)),
			"build" => json_encode(array(
				"cpu" => $server->cpu_limit,
				"controlsum" => $enginee['controlsum'],
				"engine_path" => $enginee['path']
			))
		)
	);

	Components\Page::redirect('../../settings.php?code='.$request->code);

}