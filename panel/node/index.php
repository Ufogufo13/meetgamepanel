<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \Unirest;

require_once '../../src/core/core.php';

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token'), $core->auth->getCookie('pp_server_hash')) === false) {
	Components\Page::redirect($core->settings->get('master_url').'index.php?login');
}

if($core->gsd->online() === true) {

	$response = \Unirest::get(
		"http://".$core->server->nodeData('ip').":".$core->server->nodeData('gsd_listen')."/gameservers/".$core->server->getData('gsd_id')."/log/750",
		array(
			"X-Access-Token" => $core->server->getData('gsd_secret')
		)
	);
	$content = array('contents' => $response->body);

} else {
	$content = array('contents' => 'Server is currently offline.');
}

/*
 * Display Page
 */
echo $twig->render(
	'node/index.html', array(
		'server' => array_merge($core->server->getData(), array('node' => $core->server->nodeData('node'), 'console_inner' => $content['contents'])),
		'node' => $core->server->nodeData(),
		'footer' => array(
			'seconds' => number_format((microtime(true) - $pageStartTime), 4)
		)
));
?>
