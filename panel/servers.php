<?php
/*
    MeetServPanel - GameServers Management Panel
    Copyright (c) 2015 jakub@trzasko.pl
 */
namespace PufferPanel\Core;
use \ORM as ORM;
require_once('../src/core/core.php');

if($core->auth->isLoggedIn($_SERVER['REMOTE_ADDR'], $core->auth->getCookie('pp_auth_token')) !== true){
	Components\Page::redirect('index.php?login');
	exit();
}

/*
 * Redirect
 */
if(isset($_GET['goto']) && !empty($_GET['goto']))
	$core->server->nodeRedirect($_GET['goto'], $core->user->getData('id'), $core->user->getData('root_admin'));

$networkId = ORM::forTable('users')->select('network_id')
	    ->where('users.id', $core->user->getData('id'))
	    ->findOne();
/*
 * Get the Servers
 */

if($core->user->getData('root_admin') == '1')
	$servers = ORM::forTable('servers')
					->select('servers.*')->select('mc_gametypes.name', 'gametype_name')->select('mc_networks.name', 'network_name')
					->join('mc_gametypes', array('servers.gametype_id', '=', 'mc_gametypes.id'))
					->join('mc_networks', array('servers.network_id', '=', 'mc_networks.id'))
					->where('network_id', $networkId->network_id)
					->orderByDesc('gametype_id')
					->findArray();

	$servers = ORM::forTable('servers')
					->select('servers.*')->select('mc_gametypes.name', 'gametype_name')->select('mc_networks.name', 'network_name')
					->join('mc_gametypes', array('servers.gametype_id', '=', 'mc_gametypes.id'))
					->join('mc_networks', array('servers.network_id', '=', 'mc_networks.id'))
					->where('network_id', $networkId->network_id)
					->where(array('servers.owner_id' => $core->user->getData('id'), 'servers.active' => 1))
					->where_raw('servers.owner_id = ? OR servers.hash IN(?)', array($core->user->getData('id'), join(',', $core->user->listServerPermissions())))
					->orderByDesc('gametype_id')
					->findArray();


/*
 * List Servers
 */
echo $twig->render(
		'panel/servers.html', array(
			'servers' => $servers,
			'footer' => array(
				'seconds' => number_format((microtime(true) - $pageStartTime), 4)
			)
	));
?>
